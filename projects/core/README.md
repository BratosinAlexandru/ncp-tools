# Ncp Tools — Core

[![npm version](https://img.shields.io/npm/v/@ncp-tools/core.svg)](https://npmjs.com/package/@ncp-tools/core)
[![npm bundle size](https://img.shields.io/bundlephobia/minzip/@taiga-ui/core)](https://bundlephobia.com/result?p=@taiga-ui/core)


> Basic elements needed to develop components, directives, services and more without using Angular Material.

## How to install

```
npm i @taiga-ui/{cdk,core}
```
