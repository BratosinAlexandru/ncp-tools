import {ElementRef, Optional, Provider} from '@angular/core';
import {NcpModeDirective} from '@ncp-tools/core/directives/mode';
import {NCP_MODE} from '@ncp-tools/core/tokens';
import {NcpBrightness} from '@ncp-tools/core/types';
import {EMPTY, Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

export function modeFactory(
    mode: NcpModeDirective | null,
    {nativeElement}: ElementRef,
): Observable<NcpBrightness | null> {
    const mode$ = mode
        ? mode.changes$.pipe(
            startWith(null),
            map(() => mode.mode),
        )
        : EMPTY;

    return (nativeElement['$.data-mode.attr'] = mode$);
}

export const NCP_MODE_PROVIDER: Provider = {
    provide: NCP_MODE,
    deps: [[new Optional(), NcpModeDirective], ElementRef],
    useFactory: modeFactory,
};
