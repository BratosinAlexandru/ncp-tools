import {ChangeDetectorRef} from '@angular/core';
import {ncpWatch} from '@ncp-tools/cdk';
import {NcpController} from '@ncp-tools/cdk/abstract';
import {Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

export function watchedControllerProviderFactory(
    controller: NcpController,
    changeDetectorRef: ChangeDetectorRef,
    destroy$: Observable<void>,
): NcpController {
    controller.changes$.pipe(ncpWatch(changeDetectorRef), takeUntil(destroy$)).subscribe();

    return controller;
}
