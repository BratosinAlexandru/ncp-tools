import {InjectionToken} from '@angular/core';
import {NcpDropdownControllerDirective} from './dropdown-controller.directive';

export const NCP_DROPDOWN_CONTROLLER = new InjectionToken<NcpDropdownControllerDirective>(
    'a controller that controls configuration of hints',
    {
        factory: () => new NcpDropdownControllerDirective(),
    },
);
