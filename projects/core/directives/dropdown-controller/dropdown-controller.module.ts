import {NgModule} from '@angular/core';
import {NcpDropdownControllerDirective} from './dropdown-controller.directive';

@NgModule({
    declarations: [NcpDropdownControllerDirective],
    exports: [NcpDropdownControllerDirective],
})
export class DropdownControllerModule {
}
