import {ChangeDetectorRef, InjectionToken, Provider} from '@angular/core';
import {NcpDestroyService} from '@ncp-tools/cdk';
import {watchedControllerProviderFactory} from '@ncp-tools/core/providers';
import {NCP_DROPDOWN_CONTROLLER} from './dropdown-controller.token';

export const NCP_DROPDOWN_WATCHED_CONTROLLER = new InjectionToken(
    'a watched dropdown controller',
);

export const NCP_DROPDOWN_CONTROLLER_FACTORY = watchedControllerProviderFactory;
export const NCP_DROPDOWN_CONTROLLER_PROVIDER: Provider = [
    NcpDestroyService,
    {
        provide: NCP_DROPDOWN_WATCHED_CONTROLLER,
        deps: [NCP_DROPDOWN_CONTROLLER, ChangeDetectorRef, NcpDestroyService],
        useFactory: NCP_DROPDOWN_CONTROLLER_FACTORY,
    },
];
