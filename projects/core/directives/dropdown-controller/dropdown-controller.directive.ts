import {Directive, forwardRef, Input} from '@angular/core';
import {NcpController} from '@ncp-tools/cdk/abstract';
import {NCP_DEFAULT_MAX_HEIGHT, NCP_DEFAULT_MIN_HEIGHT} from '@ncp-tools/core/constants';
import {NcpDropdownWidth, NcpHorizontalDirection, NcpVerticalDirection} from '@ncp-tools/core/types';
import {NCP_DROPDOWN_CONTROLLER} from './dropdown-controller.token';

@Directive({
    selector:
'[ncpDropdownAlign], [ncpDropdownDirection], [ncpDropdownLimitWidth], [ncpDropdownMinHeight], [ncpDropdownMaxHeight], [ncpDropdownSided]',
    providers: [
        {
            provide: NCP_DROPDOWN_CONTROLLER,
            useExisting: forwardRef(() => NcpDropdownControllerDirective),
        },
    ],
})
export class NcpDropdownControllerDirective extends NcpController {
    @Input('ncpDropdownAlign')
    align: NcpHorizontalDirection = 'right';

    @Input('ncpDropdownDirection')
    direction: NcpVerticalDirection | null = null;

    @Input('ncpDropdownLimitWidth')
    limitWidth: NcpDropdownWidth = 'auto';

    @Input('ncpDropdownMinHeight')
    minHeight = NCP_DEFAULT_MIN_HEIGHT;

    @Input('ncpDropdownMaxHeight')
    maxHeight = NCP_DEFAULT_MAX_HEIGHT;

    @Input('ncpDropdownSided')
    sided = false;
}
