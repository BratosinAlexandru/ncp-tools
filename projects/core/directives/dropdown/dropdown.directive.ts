import {
    AfterViewChecked,
    ComponentFactoryResolver,
    Directive,
    ElementRef,
    forwardRef,
    Inject,
    Injector,
    Input,
    OnDestroy,
    Optional,
} from '@angular/core';
import {NcpActiveZoneDirective, NcpParentsScrollService, NcpPortalService} from '@ncp-tools/cdk';
import {NcpAbstractDropdown} from '@ncp-tools/core/abstract';
import {NcpDropdown} from '@ncp-tools/core/interfaces';
import {NCP_DROPDOWN} from '@ncp-tools/core/tokens';

@Directive({
    selector: '[ncpDropdown]:not(ng-container)',
    providers: [
        {
            provide: NCP_DROPDOWN,
            useExisting: forwardRef(() => NcpDropdownDirective),
        },
        NcpParentsScrollService,
    ],
})
export class NcpDropdownDirective
    extends NcpAbstractDropdown
    implements NcpDropdown, AfterViewChecked, OnDestroy {
    @Input('ncpDropdown')
    set open(value: boolean) {
        if (value) {
            this.openDropdownBox();
        } else {
            this.closeDropdownBox();
        }
    }

    constructor(
        @Inject(ComponentFactoryResolver)
            componentFactoryResolver: ComponentFactoryResolver,
        @Inject(Injector) injector: Injector,
        @Inject(NcpPortalService)
            portalService: NcpPortalService,
        @Inject(ElementRef) elementRef: ElementRef<HTMLElement>,
        @Inject(NcpActiveZoneDirective)
        @Optional()
            activeZone: NcpActiveZoneDirective | null,
        @Inject(NcpParentsScrollService) readonly refresh$: NcpParentsScrollService,
    ) {
        super(componentFactoryResolver, injector, portalService, elementRef, activeZone);
    }

}
