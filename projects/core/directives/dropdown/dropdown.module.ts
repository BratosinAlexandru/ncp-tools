import {NgModule} from '@angular/core';
import {NcpDropdownBoxModule} from '@ncp-tools/core/components/dropdown-box';
import {NcpDropdownDirective} from './dropdown.directive';

@NgModule({
    imports: [NcpDropdownBoxModule],
    declarations: [NcpDropdownDirective],
    exports: [NcpDropdownDirective],
})
export class NcpDropdownModule {
}
