import {Directive, Input} from '@angular/core';
import {NcpController} from '@ncp-tools/cdk/abstract';
import {NcpBrightness} from '@ncp-tools/core/types';

@Directive({
    selector: '[ncpMode]',
})
export class NcpModeDirective extends NcpController {
    @Input('ncpMode')
    mode: NcpBrightness | null = null;
}
