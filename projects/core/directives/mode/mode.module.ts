import {NgModule} from '@angular/core';
import {NcpModeDirective} from './mode.directive';

@NgModule({
    declarations: [NcpModeDirective],
    exports: [NcpModeDirective],
})
export class NcpModeModule {
}
