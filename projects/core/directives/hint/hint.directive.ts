import {Directive, ElementRef, Inject, Input, OnDestroy, Optional, Renderer2, Self} from '@angular/core';
import {NcpActiveZoneDirective, NcpDestroyService, NcpHoveredService, NcpObscuredService, NcpParentsScrollService} from '@ncp-tools/cdk';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {combineLatest, of, Subject} from 'rxjs';
import {delay, distinctUntilChanged, map, startWith, switchMap, take, takeUntil} from 'rxjs/operators';
import {AbstractNcpHint} from '@ncp-tools/core/abstract';
import {DESCRIBED_BY} from '@ncp-tools/core/directives/described-by';
import {NcpHintService} from '@ncp-tools/core/services';

export const HINT_HOVERED_CLASS = '_hint_hovered';

@Directive({
    selector: '[ncpHint]:not(ng-container)',
    providers: [NcpObscuredService, NcpParentsScrollService, NcpDestroyService],
})
export class NcpHintDirective extends AbstractNcpHint implements OnDestroy {
    public readonly componentHovered$ = new Subject<boolean>();

    @Input()
    public ncpHintId?: string;

    @Input()
    public ncpHintShowDelay = 500;

    @Input()
    public ncpHintHideDelay = 200;

    @Input()
    public ncpHintHost: HTMLElement | null = null;

    @Input()
    set ncpHint(value: PolymorpheusContent | null) {
        if (!value) {
            this.hideTooltip();
            this.content = '';
            return;
        }

        this.content = value;
    }

    constructor(
        @Inject(Renderer2) private readonly renderer: Renderer2,
        @Inject(ElementRef) elementRef: ElementRef<HTMLElement>,
        @Inject(NcpHintService) hintService: NcpHintService,
        @Inject(NcpDestroyService)
            destroy$: NcpDestroyService,
        @Inject(NcpObscuredService)
        @Self()
            obscured$: NcpObscuredService,
        @Inject(NcpHoveredService) hoveredService: NcpHoveredService,
        @Optional()
        @Inject(NcpActiveZoneDirective)
        activeZone: NcpActiveZoneDirective | null,
    ) {
        super(elementRef, hintService, activeZone);

        combineLatest(
            hoveredService.detectHoverEvents$(elementRef.nativeElement),
            this.componentHovered$.pipe(startWith(false)),
        )
            .pipe(
                map(
                    ([directiveHovered, componentHovered]) =>
                        directiveHovered || componentHovered,
                ),
                switchMap((visible) => {
                    this.toggleClass(visible);

                    return of(visible).pipe(
                        delay(visible ? this.ncpHintShowDelay : this.ncpHintHideDelay),
                    );
                }),
                switchMap((visible) =>
                    visible && this.mode !== 'overflow'
                        ? obscured$.pipe(
                            map((obscured) => !obscured),
                            take(2),
                        )
                        : of(visible),
                ),
                distinctUntilChanged(),
                takeUntil(destroy$),
            )
            .subscribe((visible) => {
                if (visible) {
                    this.showTooltip();
                } else {
                    this.hideTooltip();
                }
            });
        this.hintService.register(this);
    }

    public ngOnDestroy(): void {
        this.hintService.unregister(this);
    }

    get id(): string | null {
        return this.ncpHintId ? this.ncpHintId + DESCRIBED_BY : null;
    }

    get host(): HTMLElement {
        return this.ncpHintHost ? this.ncpHintHost : this.elementRef.nativeElement;
    }

    public getElementClientRect(): ClientRect {
        return this.host.getBoundingClientRect();
    }

    protected showTooltip(): void {
        if (this.content === '') {
            return;
        }
        this.toggleClass(true);
        this.hintService.add(this);
    }

    protected hideTooltip(): void {
        this.toggleClass(false);
        this.hintService.remove(this);
    }

    private toggleClass(add: boolean): void {
        if (add) {
            this.renderer.addClass(this.elementRef.nativeElement, HINT_HOVERED_CLASS);
        } else {
            this.renderer.removeClass(this.elementRef.nativeElement, HINT_HOVERED_CLASS);
        }
    }
}
