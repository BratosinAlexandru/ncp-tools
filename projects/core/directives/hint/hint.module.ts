import {NgModule} from '@angular/core';
import {NcpHintDirective} from './hint.directive';

@NgModule({
  declarations: [NcpHintDirective],
  exports: [NcpHintDirective],
})
export class NcpHintModule {
}
