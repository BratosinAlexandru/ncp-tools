import {NgModule} from '@angular/core';
import {NcpDescribedByDirective} from './described-by.directive';


@NgModule({
    declarations: [NcpDescribedByDirective],
    exports: [NcpDescribedByDirective],
})
export class NcpDescribedByModule {
}
