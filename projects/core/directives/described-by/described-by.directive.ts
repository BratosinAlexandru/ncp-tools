import {Directive, HostBinding, Inject, Input} from '@angular/core';
import {NcpHintService} from '@ncp-tools/core/services';
import {Observable} from 'rxjs';
import {NCP_DESCRIBED_BY_PROVIDERS, NCP_DESCRIBED_BY_SHOW} from './described-by.providers';

export const DESCRIBED_BY = '_described-by';

@Directive({
    selector: '[ncpDescribedBy]:not(ng-container)',
    providers: NCP_DESCRIBED_BY_PROVIDERS,
})
export class NcpDescribedByDirective {
    @Input()
    public ncpDescribedBy = '';

    constructor(
        @Inject(NcpHintService) hintService: NcpHintService,
        @Inject(NCP_DESCRIBED_BY_SHOW) visibility$: Observable<boolean>,
    ) {
        visibility$.subscribe((visible) => {
            if (!this.ncpDescribedBy) {
                return;
            }

            if (visible) {
                hintService.showHintForId(this.ncpDescribedBy);
            } else {
                hintService.hideHintForId(this.ncpDescribedBy);
            }
        });
    }

    @HostBinding('attr.aria-describedby')
    get computedDescribedBy(): string | null {
        return this.ncpDescribedBy ? this.ncpDescribedBy + DESCRIBED_BY : null;
    }

}

