import {NgModule} from '@angular/core';
import {NcpPointerHintDirective} from './pointer-hint.directive';

@NgModule({
  declarations: [NcpPointerHintDirective],
  exports: [NcpPointerHintDirective],
})
export class NcpPointerHintModule {
}
