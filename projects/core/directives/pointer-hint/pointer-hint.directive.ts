import {Directive, ElementRef, Inject, Input} from '@angular/core';
import {NcpDestroyService, NcpHoveredService, typedFromEvent} from '@ncp-tools/cdk';
import {AbstractNcpHint} from '@ncp-tools/core/abstract';
import {NcpHintService} from '@ncp-tools/core/services';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {Observable, of} from 'rxjs';
import {delay, distinctUntilChanged, filter, startWith, switchMap, takeUntil} from 'rxjs/operators';

@Directive({
    selector: '[ncpPointerHint]:not(ng-container)',
    providers: [NcpDestroyService],
})
export class NcpPointerHintDirective extends AbstractNcpHint {
    @Input()
    public ncpHintShowDelay = 0;

    @Input()
    public ncpHintHideDelay = 0;

    @Input()
    set ncpPointerHint(value: PolymorpheusContent | null) {
        if (!value) {
            this.hideTooltip();
            this.content = '';

            return;
        }

        this.content = value;
    }

    public content: PolymorpheusContent = '';

    private currentMouseRect = this.mousePositionToClientRect();

    constructor(
        @Inject(ElementRef) elementRef: ElementRef<HTMLElement>,
        @Inject(NcpHintService) hintService: NcpHintService,
        @Inject(NcpDestroyService)
        private readonly destroy$: NcpDestroyService,
        @Inject(NcpHoveredService) hoveredService: NcpHoveredService,
    ) {
        super(elementRef, hintService, null);

        const hint$ = hoveredService.detectHoverEvents$(this.elementRef.nativeElement).pipe(
            filter(() => !!this.content),
            startWith(false),
            distinctUntilChanged(),
        );

        hint$
            .pipe(
                switchMap((visible) =>
                    of(visible).pipe(
                        delay(visible ? this.ncpHintShowDelay : this.ncpHintHideDelay),
                    ),
                ),
                takeUntil(destroy$),
            )
            .subscribe({
                next: (visible) => {
                    if (visible) {
                        this.showTooltip();
                    } else {
                        this.hideTooltip();
                    }
                },
                complete: () => {
                    this.hideTooltip();
                },
            });

        this.initMouseMoveSubscription();
    }

    public getElementClientRect(): ClientRect {
        return this.currentMouseRect;
    }

    private initMouseMoveSubscription(): void {
        const mouseMove$: Observable<MouseEvent> = typedFromEvent(
            this.elementRef.nativeElement,
            'mousemove',
        );

        mouseMove$.pipe(takeUntil(this.destroy$)).subscribe(({clientX, clientY}) => {
            this.currentMouseRect = this.mousePositionToClientRect(clientX, clientY);
        });
    }

    private mousePositionToClientRect(x: number = 0, y: number = 0): ClientRect {
        return {
            left: x,
            right: x,
            top: y,
            bottom: y,
            width: 0,
            height: 0,
        };
    }
}
