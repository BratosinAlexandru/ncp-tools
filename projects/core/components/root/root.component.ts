import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {NCP_ANIMATIONS_DURATION} from '@ncp-tools/core/tokens';

@Component({
    selector: 'ncp-root',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    host: {
        'data-ncp-version': '0.0.1',
        '[style.--ncp-duration]': 'duration + "ms"',
    },
})
export class NcpRootComponent {
    constructor(
        @Inject(NCP_ANIMATIONS_DURATION) readonly duration: number
    ) {
    }

}
