import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NcpPortalHostModule} from '@ncp-tools/cdk/components/portal-host';
import {NcpHintsHostModule} from '@ncp-tools/core/components/hints-host';
import {NcpRootComponent} from './root.component';

@NgModule({
    imports: [
        CommonModule,
        NcpPortalHostModule,
        NcpHintsHostModule,
    ],
    declarations: [NcpRootComponent],
    exports: [NcpRootComponent],
})
export class NcpRootModule {
}
