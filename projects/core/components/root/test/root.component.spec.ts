import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NcpRootComponent, NcpRootModule} from '@ncp-tools/core/components';
import {configureTestSuite} from 'ng-bullet';

describe('root component', () => {
    let fixture: ComponentFixture<NcpRootComponent>;
    let root: HTMLElement;

    configureTestSuite(() => {
        TestBed.configureTestingModule({
            imports: [NcpRootModule, NoopAnimationsModule],
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(NcpRootComponent);
        root = fixture.nativeElement;

        fixture.detectChanges();
    });

    it('root has data-ncp-version attribute', () => {
        expect(root.dataset.ncpVersion).toBeDefined();
    });

    it('there is some data-ncp-version value', () => {
        expect(root.dataset.ncpVersion).not.toBe('undefined');
    });
});
