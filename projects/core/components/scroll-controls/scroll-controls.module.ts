import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NcpLetModule} from '@ncp-tools/cdk/directives/let';
import {NcpScrollControlsComponent} from './scroll-controls.component';
import {NcpScrollbarWrapperDirective} from './scrollbar-wrapper.directive';
import {NcpScrollbarDirective} from './scrollbar.directive';

@NgModule({
    declarations: [
        NcpScrollControlsComponent,
        NcpScrollbarDirective,
        NcpScrollbarWrapperDirective,
    ],
    imports: [
        CommonModule,
        NcpLetModule,
    ],
    exports: [NcpScrollControlsComponent],
})
export class NcpScrollControlsModule {
}
