import {AnimationOptions} from '@angular/animations';
import {DOCUMENT} from '@angular/common';
import {ChangeDetectionStrategy, Component, ElementRef, Inject, NgZone, Optional} from '@angular/core';
import {ncpZoneOptimized} from '@ncp-tools/cdk';
import {ncpFadeIn} from '@ncp-tools/core/animations';
import {NCP_MODE_PROVIDER} from '@ncp-tools/core/providers';
import {NCP_ANIMATION_OPTIONS, NCP_MODE, NCP_SCROLL_REF} from '@ncp-tools/core/tokens';
import {NcpBrightness} from '@ncp-tools/core/types';
import {ANIMATION_FRAME} from '@ng-web-apis/common';
import {Observable} from 'rxjs';
import {distinctUntilChanged, map, startWith, throttleTime} from 'rxjs/operators';

// @dynamic
@Component({
    selector: 'ncp-scroll-controls',
    templateUrl: './scroll-controls.component.html',
    styleUrls: ['./scroll-controls.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [ncpFadeIn],
    providers: [NCP_MODE_PROVIDER],
    host: {
        '($.data-mode.attr)': 'mode$',
    },
})
export class NcpScrollControlsComponent {
    readonly refresh$ = this.animationFrame$.pipe(
        throttleTime(300),
        map(() => this.scrollbars),
        startWith([false, false]),
        distinctUntilChanged((a, b) => a[0] === b[0] && a[1] === b[1]),
        ncpZoneOptimized(this.ngZone),
    );

    readonly animation = {
        value: '',
        ...this.options,
    } as const;

    constructor(
        @Inject(NCP_ANIMATION_OPTIONS) private readonly options: AnimationOptions,
        @Inject(NgZone) private readonly ngZone: NgZone,
        @Inject(DOCUMENT) private readonly documentRef: Document,
        @Optional()
        @Inject(NCP_SCROLL_REF)
        private readonly scrollRef: ElementRef<HTMLElement> | null,
        @Inject(ANIMATION_FRAME) private readonly animationFrame$: Observable<number>,
        @Inject(NCP_MODE) readonly mode$: Observable<NcpBrightness | null>,
    ) {
    }

    private get scrollbars(): [boolean, boolean] {
        const {clientHeight, scrollHeight, clientWidth, scrollWidth} = this.scrollRef
            ? this.scrollRef.nativeElement
            : this.documentRef.documentElement;

        return [
            Math.ceil((clientHeight / scrollHeight) * 100) < 100,
            Math.ceil((clientWidth / scrollWidth) * 100) < 100,
        ];
    }

}
