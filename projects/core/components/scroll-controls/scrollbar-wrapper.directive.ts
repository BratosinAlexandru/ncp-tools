import {Directive, ElementRef} from '@angular/core';
import {NCP_ELEMENT_REF} from '@ncp-tools/core/tokens';

@Directive({
    selector: '[ncpScrollbarWrapper]',
    providers: [
        {
            provide: NCP_ELEMENT_REF,
            useExisting: ElementRef,
        },
    ],
})
export class NcpScrollbarWrapperDirective {
}
