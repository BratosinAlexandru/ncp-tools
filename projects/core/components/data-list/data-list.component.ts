import {
    ChangeDetectionStrategy,
    Component,
    ContentChildren,
    ElementRef,
    forwardRef,
    HostBinding,
    HostListener,
    Inject,
    Input,
    QueryList,
    ViewEncapsulation,
} from '@angular/core';
import {isNativeFocusedIn, isPresent, itemsQueryListObservable, moveFocus, ncpPure, setNativeMouseFocused} from '@ncp-tools/cdk';
import {NcpDataListAccessor} from '@ncp-tools/core/interfaces';
import {NCP_DATA_LIST_ACCESSOR} from '@ncp-tools/core/tokens';
import {NcpDataListType} from '@ncp-tools/core/types';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {NcpOptionComponent} from './option/option.component';

// @dynamic
@Component({
    selector: 'ncp-data-list',
    templateUrl: './data-list.component.html',
    styleUrls: ['./data-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    providers: [
        {
            provide: NCP_DATA_LIST_ACCESSOR,
            useExisting: forwardRef(() => NcpDataListComponent),
        },
    ],
})
export class NcpDataListComponent<T> implements NcpDataListAccessor<T> {
    @Input()
    @HostBinding('attr.role')
    role: NcpDataListType = 'listbox';

    @Input()
    emptyContent: PolymorpheusContent = '';

    @ContentChildren(forwardRef(() => NcpOptionComponent), {descendants: true})
    private readonly options: QueryList<NcpOptionComponent<T>> = new QueryList<any>();

    private origin?: HTMLElement;

    constructor(
        @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
    ) {
    }

    @ncpPure
    get empty$(): Observable<boolean> {
        return itemsQueryListObservable(this.options).pipe(map(({length}) => !length));
    }

    getOptions(includeDisabled: boolean = false): ReadonlyArray<T> {
        return this.options
            .toArray()
            .filter(({disabled}) => includeDisabled || !disabled)
            .map(({value}) => value)
            .filter(isPresent);
    }

    @HostListener('focusin', ['$event.relatedTarget', '$event.currentTarget'])
    onFocusIn(relatedTarget: HTMLElement, currentTarget: HTMLElement) {
        if (!currentTarget.contains(relatedTarget) && !this.origin) {
            this.origin = relatedTarget;
        }
    }

    @HostListener('mousedown.prevent')
    noop() {
    }

    @HostListener('keydown.arrowDown.prevent', ['$event.target', '1'])
    @HostListener('keydown.arrowUp.prevent', ['$event.target', '-1'])
    onKeyDownArrow(current: HTMLElement, step: number) {
        const {elements} = this;

        moveFocus(elements.indexOf(current), elements, step);
    }

    onFocus(element: HTMLElement, top: boolean): void {
        const {elements} = this;

        moveFocus(top ? -1 : elements.length, elements, top ? 1 : -1);
        this.handleFocusLoss(element);
    }

    @HostListener('mouseleave', ['$event.target'])
    handleFocusLoss(element: HTMLElement): void {
        if (this.origin && isNativeFocusedIn(element)) {
            setNativeMouseFocused(this.origin, true, true);
        }
    }

    private get elements(): ReadonlyArray<HTMLElement> {
        return Array.from(this.elementRef.nativeElement.querySelectorAll('[ncpOption]'));
    }
}
