import {Directive} from '@angular/core';

@Directive({
    selector: 'ng-template[ncpDataList]',
})
export class NcpDataListDirective {}
