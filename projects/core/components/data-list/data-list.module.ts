import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {PolymorpheusModule} from '@tinkoff/ng-polymorpheus';
import {NcpDataListDropdownManagerDirective} from './data-list-dropdown-manager.directive';
import {NcpDataListComponent} from './data-list.component';
import {NcpDataListDirective} from './data-list.directive';
import {NcpOptionGroupDirective} from './option-group.directive';
import {NcpOptionComponent} from './option/option.component';

@NgModule({
    imports: [
        CommonModule,
        PolymorpheusModule,
    ],
    declarations: [
        NcpDataListComponent,
        NcpOptionComponent,
        NcpOptionGroupDirective,
        NcpDataListDirective,
        NcpDataListDropdownManagerDirective
    ],
    exports: [
        NcpDataListComponent,
        NcpOptionComponent,
        NcpOptionGroupDirective,
        NcpDataListDirective,
        NcpDataListDropdownManagerDirective,
    ],
})
export class DataListModule {
}
