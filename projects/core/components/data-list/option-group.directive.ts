import {Directive, HostBinding, Input} from '@angular/core';

@Directive({
    selector: 'ncp-option-group',
    host: {
        role: 'group',
    },
})
export class NcpOptionGroupDirective {
    @Input()
    @HostBinding('attr.data-label')
    label = '';
}
