import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    forwardRef,
    HostBinding,
    HostListener,
    Inject,
    Input,
    OnDestroy,
    Optional,
    Self,
    TemplateRef,
} from '@angular/core';
import {isNativeFocused, NcpContextWithImplicit, NcpEventWith, setNativeFocused} from '@ncp-tools/cdk';
import {NcpDropdownDirective} from '@ncp-tools/core/directives/dropdown';
import {NcpDataListHost} from '@ncp-tools/core/interfaces';
import {NCP_DATA_LIST_HOST, NCP_OPTION_CONTENT} from '@ncp-tools/core/tokens';
import {NcpOptionRole, NcpSizeL, NcpSizeXS} from '@ncp-tools/core/types';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {NcpDataListComponent} from '../data-list.component';

export function shouldFocus({
                                currentTarget,
                            }: NcpEventWith<MouseEvent, HTMLElement>): boolean {
    return !isNativeFocused(currentTarget);
}

@Component({
    selector: 'button[ncpOption], a[ncpOption]',
    templateUrl: './option.component.html',
    styleUrls: ['./option.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    host: {
        tabIndex: '-1',
        type: 'button',
        '[attr.disabled]': 'disabled || null',
    },
})
export class NcpOptionComponent<T = unknown> implements OnDestroy {
    @Input()
    @HostBinding('attr.data-size')
    size: NcpSizeXS | NcpSizeL = 'm';

    @Input()
    @HostBinding('attr.role')
    role: NcpOptionRole = 'option';

    @Input()
    disabled = false;

    @Input()
    value?: T;

    constructor(
        @Optional()
        @Inject(NCP_OPTION_CONTENT)
        readonly content: PolymorpheusContent<NcpContextWithImplicit<TemplateRef<{}>>> | null,
        @Inject(forwardRef(() => NcpDataListComponent))
        private readonly dataList: any,
        @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
        @Optional()
        @Inject(NCP_DATA_LIST_HOST)
        private readonly host: NcpDataListHost<T> | null,
        @Optional()
        @Self()
        @Inject(NcpDropdownDirective)
        readonly dropdown: NcpDropdownDirective | null,
    ) {
    }

    ngOnDestroy(): void {
        this.dataList.handleFocusLoss(this.elementRef.nativeElement);
    }

    @HostBinding('class._with-dropdown')
    get active(): boolean {
        return !!this.dropdown && !!this.dropdown.dropdownBoxRef;
    }

    @HostListener('click')
    onClick(): void {
        if (this.host && this.value !== undefined) {
            this.host.handleOption(this.value);
        }
    }

    @HostListener('init.onMouseMove', ['$event'])
    @HostListener('mousemove.silent', ['$event'])
    onMouseMove({currentTarget}: NcpEventWith<MouseEvent, HTMLElement>) {
        setNativeFocused(currentTarget, true, true);
    }

}
