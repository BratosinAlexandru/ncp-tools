import {AnimationOptions} from '@angular/animations';
import {ChangeDetectionStrategy, Component, ElementRef, Inject} from '@angular/core';
import {AbstractNcpHint} from '@ncp-tools/core/abstract';
import {ncpFadeIn} from '@ncp-tools/core/animations';
import {NcpHintDirective} from '@ncp-tools/core/directives/hint';
import {NcpHintService} from '@ncp-tools/core/services';
import {NCP_ANIMATION_OPTIONS} from '@ncp-tools/core/tokens';

@Component({
    selector: 'ncp-hints-host',
    templateUrl: './hints-host.component.html',
    styleUrls: ['./hints-host.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [ncpFadeIn],
    host: {
        'aria-live': 'polite',
    }
})
export class NcpHintsHostComponent {

    readonly animation = {
        value: '',
        ...this.options,
    } as const;

    constructor(
        @Inject(NCP_ANIMATION_OPTIONS) private readonly options: AnimationOptions,
        @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
        @Inject(NcpHintService) readonly hints$: NcpHintService,
    ) {

    }

    get clientRect(): ClientRect {
        return this.elementRef.nativeElement.getBoundingClientRect();
    }

    onHovered(hovered: boolean, directive: AbstractNcpHint): void {
        if (directive instanceof NcpHintDirective) {
            directive.componentHovered$.next(hovered);
        }
    }
}
