import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NcpActiveZoneModule, NcpHoveredModule} from '@ncp-tools/cdk';
import {PolymorpheusModule} from '@tinkoff/ng-polymorpheus';
import {NcpHintBoxModule} from './hint-box/hint-box.module';
import {NcpHintsHostComponent} from './hints-host.component';

@NgModule({
  imports: [CommonModule, PolymorpheusModule, NcpHoveredModule, NcpHintBoxModule, NcpActiveZoneModule],
  declarations: [NcpHintsHostComponent],
  exports: [NcpHintsHostComponent],
  entryComponents: [NcpHintsHostComponent],
})
export class NcpHintsHostModule {
}
