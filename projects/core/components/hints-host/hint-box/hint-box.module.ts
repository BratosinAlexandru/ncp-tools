import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NcpHintBoxComponent} from './hint-box.component';

@NgModule({
  imports: [CommonModule],
  declarations: [NcpHintBoxComponent],
  exports: [NcpHintBoxComponent]
})
export class NcpHintBoxModule {
}
