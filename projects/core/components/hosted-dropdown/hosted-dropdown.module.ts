import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NcpActiveZoneModule, NcpObscuredModule} from '@ncp-tools/cdk';
import {DropdownControllerModule, NcpDropdownModule} from '@ncp-tools/core/directives';
import {PolymorpheusModule} from '@tinkoff/ng-polymorpheus';
import {NcpHostedDropdownConnectorDirective} from './hosted-dropdown-connector.directive';
import {NcpHostedDropdownComponent} from './hosted-dropdown.component';

@NgModule({
    declarations: [NcpHostedDropdownComponent, NcpHostedDropdownConnectorDirective],
    imports: [
        CommonModule,
        NcpObscuredModule,
        PolymorpheusModule,
        NcpActiveZoneModule,
        NcpDropdownModule,
        DropdownControllerModule,
    ],
    exports: [NcpHostedDropdownComponent, NcpHostedDropdownConnectorDirective],
})
export class NcpHostedDropdownModule {
}
