import {Directive} from '@angular/core';

@Directive({
    selector: '[ncpHostedDropdownHost]',
})
export class NcpHostedDropdownConnectorDirective {}
