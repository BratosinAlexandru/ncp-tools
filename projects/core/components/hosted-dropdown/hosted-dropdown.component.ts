import {
    ChangeDetectionStrategy,
    Component,
    ContentChild,
    ElementRef,
    EventEmitter,
    forwardRef,
    HostBinding,
    HostListener,
    Inject,
    Input,
    Output,
    ViewChild,
} from '@angular/core';
import {
    getClosestFocusable,
    isEditingKey,
    isElementEditable,
    isNativeFocusedIn,
    isNativeKeyboardFocusable,
    NCP_FOCUSABLE_ACCESSOR,
    NcpActiveZoneDirective,
    NcpContextWithImplicit,
    NcpFocusableAccessor,
    NcpNativeFocusableElement,
    ncpPure,
    setNativeFocused,
} from '@ncp-tools/cdk';
import {NcpDropdownDirective} from '@ncp-tools/core/directives';
import {
    NCP_DROPDOWN_CONTROLLER_PROVIDER,
    NCP_DROPDOWN_WATCHED_CONTROLLER,
    NcpDropdownControllerDirective,
} from '@ncp-tools/core/directives/dropdown-controller';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {NcpHostedDropdownConnectorDirective} from './hosted-dropdown-connector.directive';

@Component({
    selector: 'ncp-hosted-dropdown',
    templateUrl: './hosted-dropdown.component.html',
    styleUrls: ['./hosted-dropdown.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NCP_FOCUSABLE_ACCESSOR,
            useExisting: forwardRef(() => NcpHostedDropdownComponent),
        },
        NCP_DROPDOWN_CONTROLLER_PROVIDER,
    ],
})
export class NcpHostedDropdownComponent implements NcpFocusableAccessor {
    @Input()
    content: PolymorpheusContent = '';

    @Input()
    canOpen = true;

    @Input()
    open = false;

    @Output()
    readonly openChange = new EventEmitter<boolean>();

    @Output()
    readonly focusedChange = new EventEmitter<boolean>();

    @ViewChild(NcpActiveZoneDirective)
    readonly activeZone?: NcpActiveZoneDirective;

    @ViewChild('wrapper', {read: ElementRef})
    private wrapper?: ElementRef<HTMLDivElement>;

    @ViewChild(NcpDropdownDirective)
    private dropdownDirective?: NcpDropdownDirective;

    @ContentChild(NcpHostedDropdownConnectorDirective, {read: ElementRef})
    private dropdownHost?: ElementRef<HTMLElement>;

    constructor(
        @Inject(ElementRef) private readonly elementRef: ElementRef,
        @Inject(NCP_DROPDOWN_WATCHED_CONTROLLER)
        readonly controller: NcpDropdownControllerDirective,
    ) {
    }

    get host(): HTMLElement {
        return this.dropdownHost
            ? this.dropdownHost.nativeElement
            : this.elementRef.nativeElement;
    }

    get dropdown(): HTMLElement | null {
        return !this.dropdownDirective || this.dropdownDirective.dropdownBoxRef === null
            ? null
            : this.dropdownDirective.dropdownBoxRef.location.nativeElement;
    }

    get focusableElement(): NcpNativeFocusableElement | null {
        return isNativeKeyboardFocusable(this.host)
            ? this.host
            : getClosestFocusable(this.host, false, this.elementRef.nativeElement);
    }

    get contentContext(): NcpContextWithImplicit<NcpActiveZoneDirective | undefined> {
        return this.calculateContentContext(this.activeZone);
    }

    @HostBinding('class._hosted_dropdown_focused')
    get focused(): boolean {
        return (
            isNativeFocusedIn(this.host) ||
            (this.open && !!this.wrapper && isNativeFocusedIn(this.wrapper.nativeElement))
        );
    }

    @HostListener('focusin', ['$event.target'])
    onFocusIn(target: HTMLElement): void {
        const host = this.dropdownHost
            ? this.dropdownHost.nativeElement
            : this.focusableElement || this.elementRef.nativeElement;

        if (!host.contains(target)) {
            this.updateOpen(false);
        }
    }

    @HostListener('click', ['$event.target'])
    onClick(target: HTMLElement): void {
        const host = this.focusableElement || this.host;
        const dropdownHost = this.dropdownHost ? this.dropdownHost.nativeElement : host;

        if (!this.hostEditable && dropdownHost.contains(target)) {
            this.updateOpen(!this.open);
        }
    }

    @HostListener('keydown.esc', ['$event'])
    onKeyDownEsc(event: KeyboardEvent): void {
        if (!this.canOpen || !this.open) {
            return;
        }

        event.stopPropagation();
        this.closeDropdown();
    }

    @HostListener('keydown.arrowDown', ['$event', 'true'])
    @HostListener('keydown.arrowUp', ['$event', 'false'])
    onArrow(event: KeyboardEvent, down: boolean): void {
        this.focusDropdown(event, down);
    }

    onKeydown({key, target, defaultPrevented}: KeyboardEvent): void {
        if (
            !defaultPrevented &&
            isEditingKey(key) &&
            this.hostEditable &&
            target instanceof HTMLElement &&
            !isElementEditable(target)
        ) {
            this.focusHost();
        }
    }

    onActiveZone(active: boolean): void {
        this.updateFocused(active);

        if (!active) {
            this.updateOpen(false);
        }
    }

    onHostObscured(obscured: boolean): void {
        if (obscured) {
            this.closeDropdown();
        }
    }

    updateOpen(open: boolean): void {
        if (open && !this.canOpen) {
            return;
        }

        this.open = open;
        this.openChange.emit(open);
    }

    private get hostEditable(): boolean {
        const host = this.focusableElement || this.host;

        return host instanceof HTMLElement && isElementEditable(host);
    }

    @ncpPure
    private calculateContentContext(
        $implicit?: NcpActiveZoneDirective,
    ): NcpContextWithImplicit<NcpActiveZoneDirective | undefined> {
        return {$implicit};
    }

    private focusDropdown(event: KeyboardEvent, first: boolean): void {
        const host = this.focusableElement;

        if (
            !host ||
            !(host instanceof HTMLElement) ||
            !(event.target instanceof Node) ||
            !host.contains(event.target)
        ) {
            return;
        }

        if (
            !this.wrapper ||
            !this.open ||
            this.dropdown === null ||
            !(this.wrapper.nativeElement.nextElementSibling instanceof HTMLElement)
        ) {
            this.updateOpen(true);
            if (!isElementEditable(host)) {
                event.preventDefault();
            }

            return;
        }

        const initial = first
            ? this.wrapper.nativeElement
            : this.wrapper?.nativeElement.nextElementSibling;
        const focusable = getClosestFocusable(
            initial,
            !first,
            this.wrapper.nativeElement,
        );

        if (focusable === null) {
            return;
        }

        setNativeFocused(focusable);
        event.preventDefault();
    }

    private closeDropdown(): void {
        if (this.focused) {
            this.focusHost();
        }

        this.updateOpen(false);
    }

    private focusHost(): void {
        const host = this.focusableElement;

        if (host !== null) {
            setNativeFocused(host, true, true);
        }
    }

    private updateFocused(focused: boolean): void {
        this.focusedChange.emit(focused);
    }
}
