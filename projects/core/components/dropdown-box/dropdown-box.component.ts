import {AnimationOptions} from '@angular/animations';
import {
    AfterViewChecked,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    HostBinding,
    Inject,
    NgZone,
    ViewChild,
} from '@angular/core';
import {
    getClosestElement,
    getClosestFocusable,
    inRange,
    NcpActiveZoneDirective,
    NcpDestroyService,
    NcpOverscrollModeT,
    NcpPortalHostComponent,
    ncpPure,
    ncpZoneFree,
    POLLING_TIME,
    px,
    setNativeFocused,
} from '@ncp-tools/cdk';
import {getScreenWidth} from '@ncp-tools/cdk/utils/dom';
import {ncpDropdownAnimation} from '@ncp-tools/core/animations';
import {NCP_DEFAULT_MARGIN, NCP_DEFAULT_MAX_WIDTH} from '@ncp-tools/core/constants';
import {NcpDropdownAnimation} from '@ncp-tools/core/enums';
import {NcpAnimationOptions, NcpDropdown} from '@ncp-tools/core/interfaces';
import {NCP_ANIMATION_OPTIONS, NCP_DROPDOWN} from '@ncp-tools/core/tokens';
import {NcpHorizontalDirection, NcpVerticalDirection} from '@ncp-tools/core/types';
import {ANIMATION_FRAME, WINDOW} from '@ng-web-apis/common';
import {fromEvent, merge, Observable} from 'rxjs';
import {takeUntil, throttleTime} from 'rxjs/operators';

// @dynamic
@Component({
    selector: 'ncp-dropdown-box',
    templateUrl: './dropdown-box.component.html',
    styleUrls: ['./dropdown-box.component.less'],
    changeDetection: ChangeDetectionStrategy.Default,
    providers: [NcpDestroyService],
    animations: [ncpDropdownAnimation],
})
export class NcpDropdownBoxComponent implements AfterViewChecked {
    @HostBinding('@ncpDropdownAnimation')
    dropdownAnimation!: NcpAnimationOptions;

    @ViewChild(NcpActiveZoneDirective)
    readonly activeZone?: NcpActiveZoneDirective;

    private readonly animationTop = {
        value: NcpDropdownAnimation.FadeInTop,
        ...this.options,
    };

    private readonly animationBottom = {
        value: NcpDropdownAnimation.FadeInBottom,
        ...this.options,
    };

    private preventDirectionIsTop = false;

    @ViewChild('content', {read: ElementRef})
    readonly contentElementRef?: ElementRef<HTMLElement>;

    constructor(
        @Inject(NcpDestroyService) destroy$: NcpDestroyService,
        @Inject(NgZone) ngZone: NgZone,
        @Inject(NCP_DROPDOWN) readonly directive: NcpDropdown,
        @Inject(WINDOW) private readonly windowRef: Window,
        @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
        @Inject(NcpPortalHostComponent)
        private readonly portalHost: NcpPortalHostComponent,
        @Inject(NCP_ANIMATION_OPTIONS) private readonly options: AnimationOptions,
        @Inject(ANIMATION_FRAME) animationFrame$: Observable<number>,
    ) {
        merge(
            animationFrame$.pipe(throttleTime(POLLING_TIME)),
            this.directive.refresh$,
            fromEvent(this.windowRef, 'resize'),
        )
            .pipe(ncpZoneFree(ngZone), takeUntil(destroy$))
            .subscribe(() => {
                this.calculatePositionAndSize();
            });
    }

    get overscroll(): NcpOverscrollModeT {
        return this.inModal ? 'all' : 'scroll';
    }

    @ncpPure
    getContext<T extends object>(
        context?: T,
        activeZone?: NcpActiveZoneDirective,
    ):
        | (T & { activeZone?: NcpActiveZoneDirective })
        | { activeZone?: NcpActiveZoneDirective } {
        return {...context, activeZone};
    }

    ngAfterViewChecked(): void {
        this.calculatePositionAndSize();
    }

    onTopFocus(): void {
        this.moveFocusOutside(true);
    }

    onBottomFocus(): void {
        this.moveFocusOutside(false);
    }

    @ncpPure
    private get inModal(): boolean {
        return !!getClosestElement(this.directive.host, 'ncp-dialog-host');
    }

    private calculatePositionAndSize(): void {
        const {clientRect} = this.directive;
        const {style} = this.elementRef.nativeElement;
        const hostRect = this.directive.fixed
            ? this.portalHost.fixedPositionOffset()
            : this.portalHost.clientRect;

        style.position = this.directive.fixed ? 'fixed' : 'absolute';

        this.calculateVerticalPosition(style, clientRect, hostRect);
        this.calculateHorizontalPosition(style, clientRect, hostRect);
        this.calculateWidth(style, clientRect);
    }

    private getFinalAlign(
        style: CSSStyleDeclaration,
        directiveRect: ClientRect,
    ): NcpHorizontalDirection {
        const dropdownRect = this.elementRef.nativeElement.getBoundingClientRect();
        const dropdownWidth = this.elementRef.nativeElement.offsetWidth;
        const screenWidth = getScreenWidth(this.windowRef.document);
        const isDropdownSizeHypotheticallyFitsViewport =
            directiveRect.left + dropdownWidth < screenWidth ||
            directiveRect.right - dropdownWidth > 0;
        const isDropdownSizeActuallyFitsViewport =
            dropdownRect.right <= screenWidth && dropdownRect.left >= 0;
        let finalAlign: NcpHorizontalDirection = this.directive.align;

        switch (this.directive.align) {
            case 'left':
                if (
                    isDropdownSizeHypotheticallyFitsViewport &&
                    dropdownRect.right > screenWidth
                ) {
                    finalAlign = 'right';
                }
                break;
            case 'right':
                if (
                    isDropdownSizeHypotheticallyFitsViewport &&
                    dropdownRect.left < 0
                ) {
                    finalAlign = 'left';
                }
                break;
        }

        if (style.right === 'auto' && isDropdownSizeActuallyFitsViewport) {
            finalAlign = 'left';
        }

        if (style.left === 'auto' && isDropdownSizeActuallyFitsViewport) {
            finalAlign = 'right';
        }

        return finalAlign;
    }

    private calculateHorizontalPosition(
        style: CSSStyleDeclaration,
        directiveRect: ClientRect,
        hostRect: ClientRect,
    ): void {
        const offset = this.directive.sided
            ? this.elementRef.nativeElement.getBoundingClientRect().width + NCP_DEFAULT_MARGIN
            : 0;
        const left = Math.ceil(directiveRect.left - hostRect.left - offset);
        const right = Math.floor(hostRect.right - directiveRect.right - offset);

        switch (this.getFinalAlign(style, directiveRect)) {
            case 'left':
                if (
                    right + NCP_DEFAULT_MARGIN > this.windowRef.innerWidth ||
                    inRange(left + NCP_DEFAULT_MARGIN, 0, this.windowRef.innerWidth)
                ) {
                    style.left = px(left);
                    style.right = 'auto';
                } else {
                    style.left = 'auto';
                    style.right = px(right);
                }
                break;
            case 'right':
                if (
                    inRange(right + NCP_DEFAULT_MARGIN, 0, this.windowRef.innerWidth) ||
                    left + NCP_DEFAULT_MARGIN > this.windowRef.innerWidth
                ) {
                    style.left = 'auto';
                    style.right = px(right);
                } else {
                    style.left = px(left);
                    style.right = 'auto';
                }
                break;
        }
    }

    private calculateVerticalPosition(
        style: CSSStyleDeclaration,
        directiveRect: ClientRect,
        hostRect: ClientRect,
    ): void {
        const windowHeight = this.windowRef.innerHeight;
        const boxHeightLimit = Math.min(
            this.directive.maxHeight,
            windowHeight - NCP_DEFAULT_MARGIN * 2,
        );
        const offset = this.directive.sided
            ? NCP_DEFAULT_MARGIN - directiveRect.height
            : NCP_DEFAULT_MARGIN * 2;
        const topAvailableHeight = directiveRect.top - offset;
        const bottomAvailableHeight = windowHeight - directiveRect.bottom - offset;
        const finalDirection = this.getFinalDirection(directiveRect);

        this.preventDirectionIsTop = finalDirection === 'top';

        if (finalDirection === 'top') {
            this.dropdownAnimation = this.animationBottom;

            style.maxHeight = px(Math.min(boxHeightLimit, topAvailableHeight));
            style.top = 'auto';
            style.bottom = px(
                hostRect.bottom - directiveRect.top - NCP_DEFAULT_MARGIN + offset,
            );
        } else {
            this.dropdownAnimation = this.animationTop;

            style.maxHeight = px(Math.min(boxHeightLimit, bottomAvailableHeight));
            style.top = px(directiveRect.bottom - hostRect.top - NCP_DEFAULT_MARGIN + offset);
            style.bottom = 'auto';
        }
    }

    private getFinalDirection(directiveRect: ClientRect): NcpVerticalDirection | null {
        const windowHeight = this.windowRef.innerHeight;
        const offset = this.directive.sided
            ? NCP_DEFAULT_MARGIN - directiveRect.height
            : NCP_DEFAULT_MARGIN * 2;

        const topAvailableHeight = directiveRect.top - offset;
        const bottomAvailableHeight = windowHeight - directiveRect.bottom - offset;

        let finalDirection: NcpVerticalDirection | null = null;

        switch (this.directive.direction) {
            case 'top':
                if (topAvailableHeight >= this.directive.minHeight) {
                    finalDirection = 'top';
                }
                break;
            case 'bottom':
                if (bottomAvailableHeight >= this.directive.minHeight) {
                    finalDirection = 'bottom';
                }
                break;
        }

        const boxHeightLimit = Math.min(
            this.directive.maxHeight,
            windowHeight - NCP_DEFAULT_MARGIN * 2,
        );

        if (finalDirection === null && this.contentElementRef) {
            const visualHeight = Math.min(
                this.contentElementRef.nativeElement.getBoundingClientRect().height +
                (this.elementRef.nativeElement.offsetHeight -
                    this.elementRef.nativeElement.clientHeight),
                boxHeightLimit,
            );

            if (this.preventDirectionIsTop && topAvailableHeight >= visualHeight) {
                finalDirection = 'top';
            } else if (bottomAvailableHeight >= visualHeight) {
                finalDirection = 'bottom';
            } else {
                finalDirection = bottomAvailableHeight >= topAvailableHeight ? 'bottom' : 'top';
            }
        }

        return finalDirection;
    }

    private calculateWidth(style: CSSStyleDeclaration, directiveRect: ClientRect): void {
        style.width =
            this.directive.limitMinWidth === 'fixed' && !this.directive.sided
                ? px(directiveRect.width)
                : '';

        if (this.directive.limitMinWidth === 'min' && !this.directive.sided) {
            style.minWidth = px(directiveRect.width);
            style.maxWidth = px(NCP_DEFAULT_MAX_WIDTH);

            return;
        }

        style.minWidth = '';
        style.maxWidth = '';
    }

    private moveFocusOutside(previous: boolean): void {
        const {host} = this.directive;
        const {ownerDocument} = host;
        const root = ownerDocument ? ownerDocument.body : host;

        let focusable = getClosestFocusable(host, previous, root);

        while (focusable !== null && host.contains(focusable)) {
            focusable = getClosestFocusable(focusable, previous, root);
        }

        if (focusable === null) {
            return;
        }

        setNativeFocused(focusable);
    }
}
