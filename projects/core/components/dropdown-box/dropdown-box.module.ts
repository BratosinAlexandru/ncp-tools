import {NgModule} from '@angular/core';
import {NcpActiveZoneModule, NcpOverscrollModule} from '@ncp-tools/cdk';
import {NcpScrollbarModule} from '@ncp-tools/core/components/scrollbar';
import {NcpModeModule} from '@ncp-tools/core/directives/mode';
import {PolymorpheusModule} from '@tinkoff/ng-polymorpheus';
import {NcpDropdownBoxComponent} from './dropdown-box.component';

@NgModule({
    imports: [
        NcpActiveZoneModule,
        PolymorpheusModule,
        NcpOverscrollModule,
        NcpScrollbarModule,
        NcpModeModule,
    ],
    declarations: [NcpDropdownBoxComponent],
    entryComponents: [NcpDropdownBoxComponent],
    exports: [NcpDropdownBoxComponent],
})
export class NcpDropdownBoxModule {
}
