import {ChangeDetectionStrategy, Component, ContentChild, ElementRef, HostBinding, HostListener, Inject, Input} from '@angular/core';
import {getElementOffset, isFirefox, NCP_IS_IOS} from '@ncp-tools/cdk';
import {NCP_SCROLL_REF} from '@ncp-tools/core/tokens';
import {CSS, USER_AGENT} from '@ng-web-apis/common';
import {NcpScrollableDirective} from './scrollable.directive';

export function scrollRefFactory({
                                     browserScrollRef,
                                 }: NcpScrollbarComponent): ElementRef<HTMLElement> {
    return browserScrollRef;
}

// @dynamic
@Component({
    selector: 'ncp-scrollbar',
    templateUrl: './scrollbar.component.html',
    styleUrls: ['./scrollbar.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NCP_SCROLL_REF,
            deps: [NcpScrollbarComponent],
            useFactory: scrollRefFactory,
        },
    ],
})
export class NcpScrollbarComponent {
    @Input()
    hidden = false;

    @HostBinding('class._container')
    @ContentChild(NcpScrollableDirective, {read: ElementRef})
    readonly scrollable?: ElementRef<HTMLDivElement>;

    private readonly isLegacy: boolean =
        !this.cssRef.supports('position', 'sticky') ||
        (isFirefox(this.userAgent) && !this.cssRef.supports('scrollbar-width', 'none'));

    constructor(
        @Inject(CSS) private readonly cssRef: any,
        @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>,
        @Inject(USER_AGENT) private readonly userAgent: string,
        @Inject(NCP_IS_IOS) private readonly isIos: boolean,
    ) {
    }

    get showScrollbars(): boolean {
        return !this.hidden && !this.isIos && (!this.isLegacy || !!this.scrollable);
    }

    get browserScrollRef(): ElementRef<HTMLElement> {
        return this.scrollable || this.elementRef;
    }

    @HostBinding('class._legacy')
    get showNative(): boolean {
        return this.isLegacy && !this.hidden && !this.scrollable;
    }

    @HostListener('tui-scroll-into-view', ['$event'])
    scrollIntoView(event: CustomEvent<HTMLElement>): void {
        const {detail} = event;
        const {nativeElement} = this.browserScrollRef;

        event.stopPropagation();

        const {offsetTop, offsetLeft} = getElementOffset(nativeElement, detail);

        nativeElement.scrollTop =
            offsetTop + detail.offsetHeight / 2 - nativeElement.clientHeight / 2;
        nativeElement.scrollLeft =
            offsetLeft + detail.offsetWidth / 2 - nativeElement.clientWidth / 2;
    }
}
