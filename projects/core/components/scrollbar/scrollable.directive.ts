import {Directive} from '@angular/core';

@Directive({
    selector: '[ncpScrollable]',
})
export class NcpScrollableDirective {}
