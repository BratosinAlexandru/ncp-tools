import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NcpScrollControlsModule} from '@ncp-tools/core/components/scroll-controls';
import {NcpScrollRefDirective} from './scroll-ref.directive';
import {NcpScrollableDirective} from './scrollable.directive';
import {NcpScrollbarComponent} from './scrollbar.component';

@NgModule({
    declarations: [
        NcpScrollbarComponent,
        NcpScrollRefDirective,
        NcpScrollableDirective,
    ],
    imports: [
        CommonModule,
        NcpScrollControlsModule,
    ],
    exports: [
        NcpScrollbarComponent,
        NcpScrollRefDirective,
        NcpScrollableDirective,
    ],
})
export class NcpScrollbarModule {
}
