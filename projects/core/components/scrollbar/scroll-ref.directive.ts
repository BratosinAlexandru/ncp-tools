import {Directive, ElementRef} from '@angular/core';
import {NCP_SCROLL_REF} from '@ncp-tools/core/tokens';

export const SCROLL_REF_SELECTOR = '[ncpScrollRef]';

@Directive({
    selector: SCROLL_REF_SELECTOR,
    providers: [
        {
            provide: NCP_SCROLL_REF,
            useExisting: ElementRef,
        },
    ],
})
export class NcpScrollRefDirective {}
