import {animate, style, transition, trigger} from '@angular/animations';
import {NcpDropdownAnimation} from '@ncp-tools/core/enums';

const TRANSITION = '{{duration}}ms ease-in-out';
const DURATION = {params: {duration: 300}};

export const ncpFadeIn = trigger('ncpFadeIn', [
  transition(
    ':enter',
    [style({opacity: 0}), animate(TRANSITION, style({opacity: 1}))],
    DURATION,
  ),
  transition(
    ':leave',
    [style({opacity: 1}), animate(TRANSITION, style({opacity: 0}))],
    DURATION,
  ),
]);

export const ncpDropdownAnimation = trigger('ncpDropdownAnimation', [
    transition(
        `* => ${NcpDropdownAnimation.FadeInTop}`,
        [
            style({transform: 'translateY(-10px)', opacity: 0}),
            animate(TRANSITION, style({transform: 'translateY(0)', opacity: 1})),
        ],
        DURATION,
    ),
    transition(
        `* => ${NcpDropdownAnimation.FadeInBottom}`,
        [
            style({transform: 'translateY(10px)', opacity: 0}),
            animate(TRANSITION, style({transform: 'translateY(0)', opacity: 1})),
        ],
        DURATION,
    ),
    transition(
        `${NcpDropdownAnimation.FadeInBottom} => *`,
        [
            style({transform: 'translateY(0)', opacity: 1}),
            animate(TRANSITION, style({transform: 'translateY(10px)', opacity: 0})),
        ],
        DURATION,
    ),
    transition(
        `${NcpDropdownAnimation.FadeInTop} => *`,
        [
            style({transform: 'translateY(0)', opacity: 1}),
            animate(TRANSITION, style({transform: 'translateY(-10px)', opacity: 0})),
        ],
        DURATION,
    ),
]);
