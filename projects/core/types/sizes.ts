export type NcpSizeM = 'm';
export type NcpSizeS = 's' | NcpSizeM;
export type NcpSizeXS = 'xs' | NcpSizeS;
export type NcpSizeL = 'l' | NcpSizeM;
export type NcpSizeXL = 'xl' | NcpSizeL;
export type NcpSizeXXL = 'xxl' | NcpSizeXL;
