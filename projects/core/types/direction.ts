export type NcpHorizontalDirection = 'left' | 'right';

export type NcpVerticalDirection = 'top' | 'bottom';

export type NcpSideDirection = 'bottom-left' | 'bottom-right' | 'top-left' | 'top-right';

export type NcpDirection = NcpHorizontalDirection | NcpSideDirection;
