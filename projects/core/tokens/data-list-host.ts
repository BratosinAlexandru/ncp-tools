import {InjectionToken} from '@angular/core';
import {NcpDataListHost} from '@ncp-tools/core/interfaces';

export const NCP_DATA_LIST_HOST = new InjectionToken<NcpDataListHost<any>>(
    'data list controller',
);
