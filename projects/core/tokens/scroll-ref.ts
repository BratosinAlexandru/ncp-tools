import {ElementRef, InjectionToken} from '@angular/core';

export const NCP_SCROLL_REF = new InjectionToken<ElementRef<HTMLElement>>(
    'representing a scrollable container',
);
