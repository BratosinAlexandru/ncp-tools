import {InjectionToken} from '@angular/core';
import {NcpDropdown} from '@ncp-tools/core/interfaces';

export const NCP_DROPDOWN = new InjectionToken<NcpDropdown>(
    'controlling NcpDropdownBoxComponent',
);
