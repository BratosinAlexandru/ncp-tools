import {ElementRef, InjectionToken} from '@angular/core';

export const NCP_ELEMENT_REF = new InjectionToken<ElementRef>(
    'element ref for when you cannot use @Input for single time injection',
);
