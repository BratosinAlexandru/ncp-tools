import {InjectionToken} from '@angular/core';

export const NCP_ANIMATIONS_DURATION = new InjectionToken<number>(
  'Duration of all animations in ms',
  {
    factory: () => 300,
  }
);
