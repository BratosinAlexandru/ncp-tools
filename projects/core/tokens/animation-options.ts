import {AnimationOptions} from '@angular/animations';
import {inject, InjectionToken} from '@angular/core';
import {NCP_ANIMATIONS_DURATION} from './animations-duration';

export const NCP_ANIMATION_OPTIONS = new InjectionToken<AnimationOptions>(
    'Options for animations',
    {
        factory: () => ({
            params: {
                duration: inject(NCP_ANIMATIONS_DURATION),
            },
        }),
    },
);
