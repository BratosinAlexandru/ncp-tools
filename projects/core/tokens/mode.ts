import {InjectionToken} from '@angular/core';
import {NcpBrightness} from '@ncp-tools/core/types';
import {Observable} from 'rxjs';

export const NCP_MODE: InjectionToken<Observable<NcpBrightness | null>> =
    new InjectionToken<Observable<NcpBrightness | null>>(
        'mode stream',
    );
