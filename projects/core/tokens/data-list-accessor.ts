import {InjectionToken} from '@angular/core';
import {NcpDataListAccessor} from '@ncp-tools/core/interfaces';

export const NCP_DATA_LIST_ACCESSOR = new InjectionToken<NcpDataListAccessor>(
    'Data list accessor for options',
);
