import {InjectionToken, TemplateRef} from '@angular/core';
import {NcpContextWithImplicit} from '@ncp-tools/cdk';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';

export const NCP_OPTION_CONTENT = new InjectionToken<PolymorpheusContent<NcpContextWithImplicit<TemplateRef<{}>>>>(
    'content for NcpOption',
);
