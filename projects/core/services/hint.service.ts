import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

type NcpHintDirective = any;
type AbstractNcpHint = any;

@Injectable({
  providedIn: 'root'
})
export class NcpHintService extends BehaviorSubject<ReadonlyArray<AbstractNcpHint>> {
  constructor() {
    super([]);
  }

  private directives: ReadonlyArray<NcpHintDirective> = [];

  add(directive: AbstractNcpHint): void {
    this.next(this.value.concat(directive));
  }

  remove(directive: AbstractNcpHint): void {
    if (this.value.includes(directive)) {
      this.next(this.value.filter(hint => hint !== directive));
    }
  }

  register(directive: NcpHintDirective): void {
    this.directives = [...this.directives, directive];
  }

  unregister(directive: NcpHintDirective): void {
    this.remove(directive);
    this.directives = this.directives.filter(dir => dir !== directive);
  }

  showHintForId(id: string): void {
    const directive = this.findDirectiveWithHintId(id);

    if (directive) {
      this.add(directive);
    }
  }

  hideHintForId(id: string): void {
    const directive = this.findDirectiveWithHintId(id);

    if (directive) {
      this.remove(directive);
    }
  }

  private findDirectiveWithHintId(id: string): NcpHintDirective | undefined {
    return this.directives.find(directive => directive.ncpHintId === id);
  }
}
