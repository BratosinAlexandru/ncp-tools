export const NCP_DEFAULT_MARGIN = 4;
export const NCP_DEFAULT_MIN_HEIGHT = 80;
export const NCP_DEFAULT_MAX_HEIGHT = 400;
export const NCP_DEFAULT_MAX_WIDTH = 600;
