export const enum NcpDropdownAnimation {
    FadeInBottom = 'fadeInBottom',
    FadeInTop = 'fadeInTop',
}
