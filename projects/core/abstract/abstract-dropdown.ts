import {AfterViewChecked, ComponentFactoryResolver, ComponentRef, Directive, ElementRef, Injector, Input, OnDestroy} from '@angular/core';
import {checkFixedPosition, NcpActiveZoneDirective, NcpPortalService, ncpPure} from '@ncp-tools/cdk';
import {NcpDropdownBoxComponent} from '@ncp-tools/core/components/dropdown-box';
import {NCP_DEFAULT_MAX_HEIGHT, NCP_DEFAULT_MIN_HEIGHT} from '@ncp-tools/core/constants';
import {NcpDropdown} from '@ncp-tools/core/interfaces';
import {NcpDropdownWidth, NcpHorizontalDirection, NcpVerticalDirection} from '@ncp-tools/core/types';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {Observable} from 'rxjs';

@Directive()
export abstract class NcpAbstractDropdown
    implements NcpDropdown, AfterViewChecked, OnDestroy {
    @Input('ncpDropdownContent')
    content: PolymorpheusContent = '';

    @Input('ncpDropdownHost')
    ncpDropdownHost: HTMLElement | null = null;

    @Input('ncpDropdownMinHeight')
    minHeight = NCP_DEFAULT_MIN_HEIGHT;

    @Input('ncpDropdownMaxHeight')
    maxHeight = NCP_DEFAULT_MAX_HEIGHT;

    @Input('ncpDropdownAlign')
    align: NcpHorizontalDirection = 'left';

    @Input('ncpDropdownDirection')
    direction: NcpVerticalDirection | null = null;

    @Input('ncpDropdownSided')
    sided = false;

    @Input('ncpDropdownLimitWidth')
    limitMinWidth: NcpDropdownWidth = 'min';

    dropdownBoxRef: ComponentRef<NcpDropdownBoxComponent> | null = null;

    abstract refresh$: Observable<unknown>;

    constructor(
        private readonly componentFactoryResolver: ComponentFactoryResolver,
        private readonly injector: Injector,
        private readonly portalService: NcpPortalService,
        protected readonly elementRef: ElementRef<HTMLElement>,
        readonly activeZone: NcpActiveZoneDirective | null,
    ) {
    }

    ngOnDestroy(): void {
        this.closeDropdownBox();
    }

    ngAfterViewChecked(): void {
        if (this.dropdownBoxRef !== null) {
            this.dropdownBoxRef.changeDetectorRef.detectChanges();
            this.dropdownBoxRef.changeDetectorRef.markForCheck();
        }
    }

    get clientRect(): ClientRect {
        return this.elementRef.nativeElement.getBoundingClientRect();
    }

    get host(): HTMLElement {
        return this.ncpDropdownHost || this.elementRef.nativeElement;
    }

    @ncpPure
    get fixed(): boolean {
        return checkFixedPosition(this.elementRef.nativeElement);
    }

    protected openDropdownBox(): void {
        if (this.dropdownBoxRef !== null) {
            return;
        }

        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
            NcpDropdownBoxComponent,
        );

        this.dropdownBoxRef = this.portalService.add(componentFactory, this.injector);
        this.dropdownBoxRef.changeDetectorRef.detectChanges();
    }

    protected closeDropdownBox(): void {
        if (this.dropdownBoxRef === null) {
            return;
        }

        this.portalService.remove(this.dropdownBoxRef);
        this.dropdownBoxRef = null;
    }
}
