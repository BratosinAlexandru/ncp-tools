import {Directive, ElementRef, Input, OnDestroy} from '@angular/core';
import {NcpHintService} from '@ncp-tools/core/services';
import {NcpDirection, NcpHintModeT} from '@ncp-tools/core/types';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {NcpActiveZoneDirective} from '@ncp-tools/cdk';

@Directive()
export abstract class AbstractNcpHint implements OnDestroy {
    @Input('ncpHintMode')
    mode: NcpHintModeT | null = null;

    @Input('ncpHintDirection')
    direction: NcpDirection = 'bottom-left';

    content: PolymorpheusContent = '';

    constructor(
        protected readonly elementRef: ElementRef<HTMLElement>,
        protected readonly hintService: NcpHintService,
        readonly activeZone: NcpActiveZoneDirective | null,
    ) {
    }

    ngOnDestroy(): void {
        this.hideTooltip();
    }

    abstract getElementClientRect(): ClientRect;

    protected showTooltip(): void {
        this.hintService.add(this);
    }

    protected hideTooltip(): void {
        this.hintService.remove(this);
    }
}
