export interface NcpDataListAccessor<T = unknown> {
    getOptions(includeDisabled?: boolean): ReadonlyArray<T>;
}
