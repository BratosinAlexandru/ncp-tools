import {AnimationOptions} from '@angular/animations';

export interface NcpAnimationOptions extends AnimationOptions {
    readonly value: string;
}
