import {NcpIdentityMatcher} from '@ncp-tools/cdk/types/matcher';

export interface NcpDataListHost<T> {
    handleOption(option: T): void;

    checkOption?(option: T): void;

    readonly identityMatcher?: NcpIdentityMatcher<T>;
}
