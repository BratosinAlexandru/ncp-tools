import {NcpActiveZoneDirective} from '@ncp-tools/cdk';
import {NcpDropdownWidth, NcpHorizontalDirection, NcpVerticalDirection} from '@ncp-tools/core/types';
import {PolymorpheusContent} from '@tinkoff/ng-polymorpheus';
import {Observable} from 'rxjs';

export interface NcpDropdown<C = object> {
    refresh$: Observable<any>;
    clientRect: ClientRect;
    content: PolymorpheusContent;
    host: HTMLElement;
    align: NcpHorizontalDirection;
    minHeight: number;
    maxHeight: number;
    direction?: NcpVerticalDirection | null;
    limitMinWidth?: NcpDropdownWidth;
    sided?: boolean;
    fixed?: boolean;
    activeZone?: NcpActiveZoneDirective | null;
    context?: C;
}
