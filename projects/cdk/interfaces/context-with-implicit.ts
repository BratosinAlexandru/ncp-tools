export interface NcpContextWithImplicit<T> {
    $implicit: T;
}
