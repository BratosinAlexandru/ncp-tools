export interface NcpNativeFocusableElement extends Element, HTMLOrSVGElement {
}

export interface NcpFocusableAccessor {
    focusableElement: NcpNativeFocusableElement | null;
    focused: boolean;
}
