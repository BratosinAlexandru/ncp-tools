import {getActualTarget} from '@ncp-tools/cdk/utils';

describe('getting actual target', () => {
    it('actual target from composedPath', () => {
        const event = new MouseEvent('click');
        const target = document.createElement('button');

        Object.defineProperty(event, 'composedPath', {value: () => [target]});

        expect(getActualTarget(event)).toEqual(target);
    });

    it('actual target it event.target', () => {
        const target = document.createElement('button');
        const event = {target: target} as any;

        Object.defineProperty(event, 'target', {value: target});

        expect(getActualTarget(event as Event)).toEqual(target);
    });
});
