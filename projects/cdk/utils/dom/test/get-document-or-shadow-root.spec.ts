import {getDocumentOrShadowRoot} from '@ncp-tools/cdk/utils';

describe('get element or shadow root', () => {
    it('node not in body', () => {
        const element = document.createElement('div');

        expect(getDocumentOrShadowRoot(element)).toEqual(
            element.ownerDocument as Document,
        );
    });

    it('node in body', () => {
        const element = document.createElement('div');

        document.body.appendChild(element);

        expect(getDocumentOrShadowRoot(element)).toEqual(
            element.getRootNode() as Document,
        );

        document.body.removeChild(element);
    });
});
