import {getElementObscurers} from '@ncp-tools/cdk/utils';

describe('element obscures', () => {
    it('returns null if there is no default view', () => {
        const element = {ownerDOcument: null};

        expect(getElementObscurers(element as any)).toEqual(null);
    });

    it('returns an edges array with obscurers', () => {
        const element = document.createElement('div');

        const result = getElementObscurers(element);

        expect(result ? result.length : null).toEqual(4);
    });
});
