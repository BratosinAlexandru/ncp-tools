import {setNativeFocused} from './set-native-focused';

export function setNativeMouseFocused(
    element: HTMLElement,
    focused: boolean = true,
    preventScroll: boolean = false,
): void {
    if (!element.ownerDocument) {
        return;
    }

    if (typeof Event === 'function') {
        element.dispatchEvent(new Event('mousedown', {bubbles: true, cancelable: true}));
    } else {
        const event = element.ownerDocument.createEvent('Event');

        event.initEvent('mousedown', true, true);
        element.dispatchEvent(event);
    }

    setNativeFocused(element, focused, preventScroll);
}
