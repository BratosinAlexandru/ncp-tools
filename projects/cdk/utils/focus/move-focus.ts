import {isNativeFocused} from './is-native-focused';
import {setNativeFocused} from './set-native-focused';

export function moveFocus(
    currentIndex: number,
    elements: ReadonlyArray<HTMLElement>,
    step: number,
): void {
    currentIndex += step;

    while (currentIndex >= 0 && currentIndex < elements.length) {
        setNativeFocused(elements[currentIndex]);

        if (isNativeFocused(elements[currentIndex])) {
            return;
        }

        currentIndex += step;
    }
}
