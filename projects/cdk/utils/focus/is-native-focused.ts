import {getNativeFocused} from './get-native-focused';

export function isNativeFocused(node: Node | null): boolean {
    return (
        !!node && !!node.ownerDocument && getNativeFocused(node.ownerDocument) === node
    );
}
