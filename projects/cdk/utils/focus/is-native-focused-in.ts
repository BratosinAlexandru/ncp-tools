import {getNativeFocused} from './get-native-focused';

export function isNativeFocusedIn(node: Node): boolean {
    if (!node.ownerDocument || !node.contains) {
        return false;
    }

    const nativeFocused = getNativeFocused(node.ownerDocument);

    return nativeFocused !== null && node.contains(nativeFocused);
}
