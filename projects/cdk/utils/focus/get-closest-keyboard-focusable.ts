import {svgNodeFilter} from '@ncp-tools/cdk/constants';
import {isNativeKeyboardFocusable} from './is-native-keyboard-focusable';
import {isNativeMouseFocusable} from './is-native-mouse-focusable';

export function getClosestFocusable(
    initial: HTMLElement,
    prev: boolean = false,
    root: Node,
    keyboard: boolean = true,
): HTMLElement | null {
    if (!root.ownerDocument) {
        return null;
    }

    const check = keyboard ? isNativeKeyboardFocusable : isNativeMouseFocusable;

    const treeWalker = root.ownerDocument.createTreeWalker(
        root,
        NodeFilter.SHOW_ELEMENT,
        svgNodeFilter,
        false,
    );

    treeWalker.currentNode = initial;

    while (prev ? treeWalker.previousNode() : treeWalker.nextNode()) {
        if (treeWalker.currentNode instanceof HTMLElement) {
            initial = treeWalker.currentNode;
        }

        if (check(initial)) {
            return initial;
        }
    }
    return null;
}
