import {QueryList} from '@angular/core';

export function originalArrayQueryList<T>(
    queryList: QueryList<T>,
): ReadonlyArray<T> {
    let array: ReadonlyArray<T> = [];

    queryList.find((_item, _index, originalArray) => {
        array = originalArray;

        return true;
    });

    return array;
}
