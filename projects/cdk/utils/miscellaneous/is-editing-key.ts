import {ncpEditingKeys} from '@ncp-tools/cdk/constants';

export function isEditingKey(key: string): boolean {
    return key.length === 1 || ncpEditingKeys.indexOf(key) !== -1;
}
