export function inRange(value: number, fromInclude: number, toExclude: number): boolean {
    return value >= fromInclude && value < toExclude;
}
