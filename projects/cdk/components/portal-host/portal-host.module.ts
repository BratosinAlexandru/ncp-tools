import {NgModule} from '@angular/core';
import {NcpPortalHostComponent} from './portal-host.component';

@NgModule({
    declarations: [NcpPortalHostComponent],
    exports: [NcpPortalHostComponent],
})
export class NcpPortalHostModule {
}
