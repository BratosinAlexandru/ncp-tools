import {Component, ViewChild} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {PolymorpheusModule} from '@tinkoff/ng-polymorpheus';
import {configureTestSuite} from 'ng-bullet';
import {NcpPortalHostComponent} from '../portal-host.component';
import {NcpPortalHostModule} from '../portal-host.module';

describe('portal host component', () => {
    @Component({
        template: `
            <ncp-portal-host>
                <button>Button</button>
            </ncp-portal-host>
        `,
    })
    class TestComponent {
        @ViewChild(NcpPortalHostComponent)
        portalHost?: NcpPortalHostComponent;
    }

    let fixture: ComponentFixture<TestComponent>;
    let testComponent: TestComponent;

    configureTestSuite(() => {
        TestBed.configureTestingModule({
            imports: [NoopAnimationsModule, PolymorpheusModule, NcpPortalHostModule],
            declarations: [TestComponent],
        });
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TestComponent);
        testComponent = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('calculates clientRect', () => {
        expect(testComponent.portalHost!.clientRect.top).toBeGreaterThanOrEqual(0);
    });

    it('calculates fixedPositionOffset', () => {
        expect(testComponent.portalHost!.fixedPositionOffset().top).toBeGreaterThanOrEqual(0);
    });
});
