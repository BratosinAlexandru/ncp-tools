import {NcpPortalService} from '../portal.service';

describe('portal service', () => {
    let service: NcpPortalService;

    beforeEach(() => {
        service = new NcpPortalService();
    });

    it('template removing', () => {
        let called = 0;
        const viewRefStub: any = {destroy: () => called++};

        service.removeTemplate(viewRefStub);
    });

    it('host view removing', () => {
        let called = 0;
        const componentRefStub: any = {hostView: {destroy: () => called++}};

        service.remove(componentRefStub);
    });

    it('throws an error with no host', () => {
        const a: any = null;
        const b: any = null;

        try {
            service.add(a, b);
            fail();
        } catch (e) {
            expect(e).toBeTruthy();
        }
    });

    it('add template child with host attached', () => {
        const a: any = null;
        const result = {};
        const componentPortalStub: any = {
            addTemplateChild: () => result,
        };

        service.attach(componentPortalStub);

        expect(service.addTemplate(a)).toBe(result as any);
    });
});
