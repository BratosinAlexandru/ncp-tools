import {ComponentFactory, ComponentRef, EmbeddedViewRef, Injectable, Injector, TemplateRef} from '@angular/core';
import {NcpPortalHostComponent} from './portal-host.component';

const NO_HOST = 'Portals cannot be used without NcpPortalHostComponent.';

@Injectable({
    providedIn: 'root',
})
export class NcpPortalService {
    private host?: NcpPortalHostComponent;

    private get safeHost(): NcpPortalHostComponent {
        if (!this.host) {
            throw new Error(NO_HOST);
        }

        return this.host;
    }

    attach(host: NcpPortalHostComponent): void {
        this.host = host;
    }

    add<C>(componentFactory: ComponentFactory<C>, injector: Injector): ComponentRef<C> {
        return this.safeHost.addComponentChild(componentFactory, injector);
    }

    remove<C>({hostView}: ComponentRef<C>): void {
        hostView.destroy();
    }

    addTemplate<C>(templateRef: TemplateRef<C>, context?: C): EmbeddedViewRef<C> {
        return this.safeHost.addTemplateChild(templateRef, context);
    }

    removeTemplate<C>(viewRef: EmbeddedViewRef<C>): void {
        viewRef.destroy();
    }
}
