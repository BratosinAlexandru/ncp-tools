export interface NcpTypedEventTarget<E> {
  addEventListener(
    type: string,
    listener: ((evt: E) => void) | null,
    options?: boolean | AddEventListenerOptions,
  ): void;
  removeEventListener(
    type: string,
    listener?: ((evt: E) => void) | null,
    options?: boolean | EventListenerOptions,
  ): void;
}

export type NcpEventWith<G extends Event, T extends NcpTypedEventTarget<G>> = G & {
  readonly currentTarget: T;
};
