import {NcpStringHandler} from './handlers';
import {NcpMapper} from './mapper';

export type NcpMatcher<I> = NcpMapper<I, boolean>;

export type NcpStringMatcher<I> = (
    item: I,
    matchValue: string,
    stringify: NcpStringHandler<I>,
) => boolean;

export type NcpIdentityMatcher<I> = (item1: I, item2: I) => boolean;
