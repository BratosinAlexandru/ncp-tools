export type NcpHandler<T, G> = (item: T) => G;
export type NcpBooleanHandler<T> = NcpHandler<T, boolean>;
export type NcpStringHandler<T> = NcpHandler<T, string>;
export type NcpNumberHandler<T> = NcpHandler<T, number>;
