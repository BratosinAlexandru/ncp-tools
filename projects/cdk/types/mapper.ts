export type NcpMapper<T, G> = (item: T, ...args: any[]) => G;
