import {fromEvent, Observable} from 'rxjs';
import {NcpEventWith, NcpTypedEventTarget} from '@ncp-tools/cdk/types';


export function typedFromEvent<E extends keyof WindowEventMap>(
    target: Window,
    event: E,
    options?: AddEventListenerOptions,
): Observable<NcpEventWith<WindowEventMap[E], typeof target>>;

export function typedFromEvent<E extends keyof DocumentEventMap>(
    target: Document,
    event: E,
    options?: AddEventListenerOptions,
): Observable<NcpEventWith<DocumentEventMap[E], typeof target>>;

export function typedFromEvent<T extends Element, E extends keyof HTMLElementEventMap>(
    target: T,
    event: E,
    options?: AddEventListenerOptions,
): Observable<NcpEventWith<HTMLElementEventMap[E], typeof target>>;

export function typedFromEvent<E extends Event,
    T extends NcpTypedEventTarget<NcpEventWith<E, T>>>(
    target: T,
    event: string,
    options?: AddEventListenerOptions,
): Observable<NcpEventWith<E, T>>;

export function typedFromEvent<E extends Event>(
    target: NcpTypedEventTarget<E>,
    event: string,
    options?: AddEventListenerOptions,
): Observable<E>;

export function typedFromEvent<E extends Event>(
    target: NcpTypedEventTarget<E>,
    event: string,
    options: AddEventListenerOptions = {},
): Observable<E> {
    return fromEvent(target, event, options);
}
