import {OperatorFunction, pipe, timer} from 'rxjs';
import {distinctUntilChanged, mapTo, startWith, switchMapTo} from 'rxjs/operators';

export function ncpIsAlive(lifespan: number = 0): OperatorFunction<any, boolean> {
    return pipe(
        switchMapTo(timer(lifespan).pipe(mapTo(false), startWith(true))),
        distinctUntilChanged(),
    );
}
