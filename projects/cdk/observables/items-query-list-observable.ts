import {QueryList} from '@angular/core';
import {originalArrayQueryList} from '@ncp-tools/cdk/utils/miscellaneous';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

export function itemsQueryListObservable<T>(
    queryList: QueryList<T>,
): Observable<ReadonlyArray<T>> {
    return queryList.changes.pipe(
        map(() => originalArrayQueryList(queryList)),
        startWith (originalArrayQueryList(queryList)),
    );
}
