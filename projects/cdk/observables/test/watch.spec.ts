import {fakeAsync, tick} from '@angular/core/testing';
import {ncpWatch} from '@ncp-tools/cdk/observables';
import {Subject} from 'rxjs';

describe('Watch operator function', () => {
    let $: Subject<any>;
    let called = 0;

    const stub: any = {
        markForCheck: () => called++,
    };

    beforeEach(() => {
        $ = new Subject<any>();
        called = 0;
    });

    it('initially emits nothing, after event emits "true" and after a tick emits "false"', fakeAsync(() => {
        $.pipe(ncpWatch(stub)).subscribe();

        $.next();
        tick();

        expect(called).toBe(1);
    }));
});
