import {fakeAsync, tick} from '@angular/core/testing';
import {ncpIsAlive} from '@ncp-tools/cdk/observables';
import {Subject} from 'rxjs';

describe('Observable.prototype.ncpIsAlive', () => {
    let $: Subject<any>;
    let result: boolean[];

    beforeEach(() => {
        $ = new Subject<any>();
        result = [];
    });

    it('initially emits nothing, after event emits "true" and after a tick emits "false"', fakeAsync(() => {
        $.pipe(ncpIsAlive()).subscribe(alive => {
            result.push(alive);
        });

        expect<boolean | null>(result).toEqual([]);

        $.next(99);

        expect<boolean | null>(result).toEqual([true]);

        tick();

        expect<boolean | null>(result).toEqual([true, false]);
    }));

    it('if during a lifespan another event comes, "true" is not emitted again', fakeAsync(() => {
        $.pipe(ncpIsAlive(300)).subscribe(alive => {
            result.push(alive);
        });

        $.next(11);
        tick(200);
        $.next(100);
        tick(300);

        expect<boolean | null>(result).toEqual([true, false]);
    }));
});
