import {NgZone} from '@angular/core';
import {MonoTypeOperatorFunction, Observable, pipe} from 'rxjs';

export function ncpZoneFull<T>(ngZone: NgZone): MonoTypeOperatorFunction<T> {
    return (source) =>
        new Observable((subscriber) =>
            source.subscribe({
                next: (value) => ngZone.run(() => subscriber.next(value)),
                error: (error) => ngZone.run(() => subscriber.next(error)),
                complete: () => ngZone.run(() => subscriber.complete()),
            }),
        );
}

export function ncpZoneFree<T>(ngZone: NgZone): MonoTypeOperatorFunction<T> {
    return (source) =>
        new Observable((subscriber) =>
            ngZone.runOutsideAngular(() => source.subscribe(subscriber)),
        );
}

export function ncpZoneOptimized<T>(ngZone: NgZone): MonoTypeOperatorFunction<T> {
    return pipe(ncpZoneFree(ngZone), ncpZoneFull(ngZone));
}
