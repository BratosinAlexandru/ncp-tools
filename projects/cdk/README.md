# Taiga UI — CDK

[![npm version](https://img.shields.io/npm/v/@ncp-tools/cdk.svg)](https://npmjs.com/package/@ncp-tools/cdk)
[![npm bundle size](https://img.shields.io/bundlephobia/minzip/@taiga-ui/cdk)](https://bundlephobia.com/result?p=@taiga-ui/cdk)

> Development kit consisting of the low level tools and abstractions sucs as decorators and directives used to develop reusable components in Angular without using Angular Material.

## How to install

```
npm i @taiga-ui/cdk
```
