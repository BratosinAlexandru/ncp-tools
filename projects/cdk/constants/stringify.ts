import {NcpStringHandler} from '@ncp-tools/cdk/types';

export const NCP_STRINGIFY: NcpStringHandler<any> = (item) => String(item);
