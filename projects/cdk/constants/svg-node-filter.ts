export const svgNodeFilter: NodeFilter = ((node: Node) =>
        'ownerSVGElement' in node
            ? NodeFilter.FILTER_REJECT
            : NodeFilter.FILTER_ACCEPT) as any;
