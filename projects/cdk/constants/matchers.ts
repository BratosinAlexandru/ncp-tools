import {NcpHandler} from '@ncp-tools/cdk/types';
import {NCP_STRINGIFY} from './stringify';

/**
 * a helper for matching string versions of an item
 * @param item an arbitrary element to match with a string
 * @param search searched query
 * @param stringify a handler to turn item into a string
 */
export const NCP_MATCHER = <T>(
    item: T,
    search: string,
    stringify: NcpHandler<T, string> = NCP_STRINGIFY,
) => stringify(item).toLowerCase().includes(search.toLowerCase());

/**
 * a helper for strict matching
 * @param item an arbitrary element to match with a string
 * @param search searched query
 * @param stringify a handler to turn item into a string
 */
export const NCP_STRICT_MATCHER = <T>(
    item: T,
    search: string,
    stringify: NcpHandler<T, string> = NCP_STRINGIFY,
) => stringify(item).toLowerCase() === search.toLowerCase();
