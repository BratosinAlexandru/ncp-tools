export const ncpEditingKeys: readonly string[] = [
    'Spacebar',
    'Backspace',
    'Delete',
    'ArrowLeft',
    'ArrowRight',
    'Left',
    'Right',
    'End',
    'Home',
];
