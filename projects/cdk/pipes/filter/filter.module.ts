import {NgModule} from '@angular/core';
import {NcpFilterPipe} from './filter.pipe';

@NgModule({
    declarations: [NcpFilterPipe],
    exports: [NcpFilterPipe],
})
export class NcpFilterModule {
}
