import {Pipe, PipeTransform} from '@angular/core';
import {NcpMatcher} from '@ncp-tools/cdk/types';

@Pipe({
    name: 'ncpFilter',
})
export class NcpFilterPipe implements PipeTransform {

    /**
     * filters an array using a matcher function and additional arguments
     * @param items the array which is filtered
     * @param matcher method used to filter the array
     * @param args
     */
    transform<T>(items: readonly T[], matcher: NcpMatcher<T>, ...args: any[]): T[] {
        return items.filter((item) => matcher(item, ...args));
    }

}
