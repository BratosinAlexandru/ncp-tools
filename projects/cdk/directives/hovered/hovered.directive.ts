import {Directive, ElementRef, Inject, Output} from '@angular/core';
import {NcpHoveredService} from '@ncp-tools/cdk/services';
import {Observable} from 'rxjs';

@Directive({
    selector: '[ncpHoveredChange]',
})
export class NcpHoveredDirective {
    @Output()
    readonly ncpHoveredChange: Observable<boolean>;

    constructor(
        @Inject(ElementRef) {nativeElement}: ElementRef<Element>,
        @Inject(NcpHoveredService) hoveredService: NcpHoveredService,
    ) {
        this.ncpHoveredChange = hoveredService.detectHoverEvents$(nativeElement);
    }
}
