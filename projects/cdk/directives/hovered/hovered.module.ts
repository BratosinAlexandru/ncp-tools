import {NgModule} from '@angular/core';
import {NcpHoveredDirective} from './hovered.directive';

@NgModule({
    declarations: [NcpHoveredDirective],
    exports: [NcpHoveredDirective],
})
export class NcpHoveredModule {
}
