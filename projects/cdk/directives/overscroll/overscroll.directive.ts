import {Directive, ElementRef, HostBinding, Inject, Input, NgZone} from '@angular/core';
import {ncpZoneFree, typedFromEvent} from '@ncp-tools/cdk/observables';
import {NcpDestroyService} from '@ncp-tools/cdk/services';
import {NcpEventWith, NcpOverscrollModeT} from '@ncp-tools/cdk/types';
import {canScroll, getScrollParent} from '@ncp-tools/cdk/utils/dom';
import {Observable} from 'rxjs';
import {filter, switchMap, takeUntil, tap} from 'rxjs/operators';

/**
 * @dynamic
 */
@Directive({
    selector: '[ncpOverscroll]',
    providers: [NcpDestroyService],
})
export class NcpOverscrollDirective {
    @Input('ncpOverscroll')
    mode: NcpOverscrollModeT = 'scroll';

    constructor(
        @Inject(ElementRef) {nativeElement}: ElementRef<HTMLElement>,
        @Inject(NgZone) ngZone: NgZone,
        @Inject(NcpDestroyService) destroy$: Observable<void>,
    ) {
        typedFromEvent(nativeElement, 'wheel', {passive: false})
            .pipe(
                filter(() => this.enabled),
                takeUntil(destroy$),
                ncpZoneFree(ngZone),
            ).subscribe((event) => {
            this.processEvent(
                event,
                !!event.deltaY,
                event.deltaY ? event.deltaY < 0 : event.deltaY < 0,
            );
        });

        typedFromEvent(nativeElement, 'touchstart', {passive: true})
            .pipe(
                switchMap(({touches}) => {
                    let {clientX, clientY} = touches[0];
                    let deltaX = 0;
                    let deltaY = 0;
                    let vertical: boolean;

                    return typedFromEvent(nativeElement, 'touchmove', {
                        passive: false,
                    }).pipe(
                        filter(() => this.enabled),
                        tap((event) => {
                            const changedTouch = event.changedTouches[0];

                            deltaX = clientX - changedTouch.clientX;
                            deltaY = clientY - changedTouch.clientY;
                            clientX = changedTouch.clientX;
                            clientY = changedTouch.clientY;

                            if (vertical === undefined) {
                                vertical = Math.abs(deltaY) > Math.abs(deltaX);
                            }

                            this.processEvent(
                                event,
                                vertical,
                                vertical ? deltaY < 0 : deltaX < 0,
                            );
                        }),
                    );
                }),
                takeUntil(destroy$),
                ncpZoneFree(ngZone),
            )
            .subscribe();
    }

    get enabled(): boolean {
        return this.mode !== 'none';
    }

    @HostBinding('style.overscrollBehavior')
    get overscrollBehavior(): 'contain' | null {
        return this.enabled ? 'contain' : null;
    }

    private processEvent(
        event: NcpEventWith<Event, HTMLElement>,
        vertical: boolean,
        negative: boolean,
    ): void {
        const {target, currentTarget, cancelable} = event;

        if (!cancelable || !(target instanceof Element)) {
            return;
        }

        if (
            this.mode === 'all' &&
            ((vertical && !currentTarget.contains(getScrollParent(target))) ||
                (!vertical && !currentTarget.contains(getScrollParent(target, false))))
        ) {
            event.preventDefault();
            return;
        }

        if (
            vertical &&
            ((negative && !canScroll(target, currentTarget, true, false)) ||
                (!negative && !canScroll(target, currentTarget, true, true)))
        ) {
            event.preventDefault();
            return;
        }

        if (
            !vertical &&
            ((negative && !canScroll(target, currentTarget, false, false)) ||
                (!negative && !canScroll(target, currentTarget, false, true)))
        ) {
            event.preventDefault();
        }
    }

}
