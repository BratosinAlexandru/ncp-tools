import {NgModule} from '@angular/core';
import {NcpOverscrollDirective} from './overscroll.directive';

@NgModule({
    declarations: [NcpOverscrollDirective],
    exports: [NcpOverscrollDirective],
})
export class NcpOverscrollModule {
}
