import {NgModule} from '@angular/core';
import {NcpObscuredDirective} from './obscured.directive';

@NgModule({
    declarations: [NcpObscuredDirective],
    exports: [NcpObscuredDirective],
})
export class NcpObscuredModule {
}
