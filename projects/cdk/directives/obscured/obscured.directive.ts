import {Directive, Inject, Input, Optional, Output, Self} from '@angular/core';
import {NcpActiveZoneDirective} from '@ncp-tools/cdk/directives/active-zone';
import {NcpDestroyService, NcpObscuredService, NcpParentsScrollService} from '@ncp-tools/cdk/services';
import {EMPTY, Observable, Subject} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

@Directive({
    selector: '[ncpObscured]',
    providers: [NcpObscuredService, NcpParentsScrollService, NcpDestroyService],
})
export class NcpObscuredDirective {
    private readonly enabled$ = new Subject<boolean>();

    @Input()
    set ncpObscuredEnabled(enabled: boolean) {
        this.enabled$.next(enabled);
    }

    @Output()
    readonly ncpObscured: Observable<boolean>;

    constructor(
        @Optional()
        @Inject(NcpActiveZoneDirective) activeZone: NcpActiveZoneDirective | null,
        @Self()
        @Inject(NcpObscuredService) obscured$: NcpObscuredService,
    ) {
        const mapped$ = obscured$.pipe(
            map(
                (obscuredBy) =>
                    !!obscuredBy &&
                    (!activeZone ||
                        !obscuredBy.length ||
                        obscuredBy.every((element) => !activeZone.contains(element))),
            ),
        );

        this.ncpObscured = this.enabled$.pipe(
            switchMap((enabled) => (enabled ? mapped$ : EMPTY)),
        );
    }
}
