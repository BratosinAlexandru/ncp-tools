import {Directive, ElementRef, Inject, Input, NgZone, OnDestroy, Optional, Output, SkipSelf} from '@angular/core';
import {ncpZoneOptimized} from '@ncp-tools/cdk/observables';
import {NCP_ACTIVE_ELEMENT} from '@ncp-tools/cdk/tokens';
import {Observable} from 'rxjs';
import {distinctUntilChanged, map, skip, startWith} from 'rxjs/operators';

@Directive({
    selector: '[ncpActiveZone]:not(ng-container), [ncpActiveZoneChange]:not(ng-container), [ncpActiveZoneParent]:not(ng-container)',
    exportAs: 'ncpActiveZone',
})
export class NcpActiveZoneDirective implements OnDestroy {
    private subActiveZones: ReadonlyArray<NcpActiveZoneDirective> = [];

    private ncpActiveZoneParent: NcpActiveZoneDirective | null = null;

    @Input('ncpActiveZoneParent')
    set ncpActiveZoneParentSetter(zone: NcpActiveZoneDirective | null) {
        if (this.ncpActiveZoneParent) {
            this.ncpActiveZoneParent.removeSubActiveZone(this);
        }

        if (zone) {
            zone.addSubActiveZone(this);
        }

        this.ncpActiveZoneParent = zone;
    }

    @Output()
    readonly ncpActiveZoneChange = this.active$.pipe(
        map((element) => !!element && this.contains(element)),
        startWith(false),
        distinctUntilChanged(),
        skip(1),
        ncpZoneOptimized(this.ngZone),
    );

    constructor(
        @Inject(NCP_ACTIVE_ELEMENT)
        private readonly active$: Observable<Element | null>,
        @Inject(NgZone) private readonly ngZone: NgZone,
        @Inject(ElementRef) private readonly elementRef: ElementRef<Element>,
        @Optional()
        @SkipSelf()
        @Inject(NcpActiveZoneDirective)
        private readonly directiveParentActiveZone: NcpActiveZoneDirective | null,
    ) {
        if (this.directiveParentActiveZone) {
            this.directiveParentActiveZone.addSubActiveZone(this);
        }
    }

    ngOnDestroy(): void {
        if (!!this.directiveParentActiveZone) {
            this.directiveParentActiveZone.removeSubActiveZone(this);
        }

        if (!!this.ncpActiveZoneParent) {
            this.ncpActiveZoneParent.removeSubActiveZone(this);
        }
    }

    contains(node: Node): boolean {
        return (
            this.elementRef.nativeElement.contains(node) ||
            this.subActiveZones.some(
                (item, index, array) =>
                    array.indexOf(item) === index && item.contains(node))
        );
    }

    private addSubActiveZone(activeZone: NcpActiveZoneDirective): void {
        this.subActiveZones = [...this.subActiveZones, activeZone];
    }

    private removeSubActiveZone(activeZone: NcpActiveZoneDirective): void {
        const index = this.subActiveZones.findIndex((item) => item === activeZone);

        this.subActiveZones = [
            ...this.subActiveZones.slice(0, index),
            ...this.subActiveZones.slice(index + 1),
        ];
    }
}
