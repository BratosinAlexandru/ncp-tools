import {NgModule} from '@angular/core';
import {NcpActiveZoneDirective} from './active-zone.directive';

@NgModule({
    declarations: [NcpActiveZoneDirective],
    exports: [NcpActiveZoneDirective],
})
export class NcpActiveZoneModule {
}
