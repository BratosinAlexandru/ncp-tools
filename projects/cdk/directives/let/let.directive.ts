import {Directive, Inject, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {NcpLetContext} from './let-context';

@Directive({
    selector: '[ncpLet]',
})
export class NcpLetDirective<T> {
    @Input()
    ncpLet!: T;

    constructor(
        @Inject(ViewContainerRef) viewContainerRef: ViewContainerRef,
        @Inject(TemplateRef) templateRef: TemplateRef<NcpLetContext<T>>,
    ) {
        viewContainerRef.createEmbeddedView(templateRef, new NcpLetContext<T>(this));
    }

    static ngTemplateContextGuard<T>(
        _dir: NcpLetDirective<T>,
        _ctx: any,
    ): _ctx is NcpLetDirective<T> {
        return true;
    }

}
