import {NgModule} from '@angular/core';
import {NcpLetDirective} from './let.directive';

@NgModule({
    declarations: [NcpLetDirective],
    exports: [NcpLetDirective],
})
export class NcpLetModule {
}
