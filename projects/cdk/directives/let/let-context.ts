import {NcpContextWithImplicit} from '@ncp-tools/cdk/interfaces';
import {NcpLetDirective} from './let.directive';

export class NcpLetContext<T> implements NcpContextWithImplicit<T> {
    constructor(private readonly internalDirectiveInstance: NcpLetDirective<T>) {
    }

    get $implicit(): T {
        return this.internalDirectiveInstance.ncpLet;
    }

    get ncpLet(): T {
        return this.internalDirectiveInstance.ncpLet;
    }
}
