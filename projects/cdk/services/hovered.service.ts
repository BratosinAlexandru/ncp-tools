import {DOCUMENT} from '@angular/common';
import {Inject, Injectable, NgZone} from '@angular/core';
import {ncpZoneOptimized, typedFromEvent} from '@ncp-tools/cdk/observables';
import {getActualTarget} from '@ncp-tools/cdk/utils/dom';
import {merge, Observable} from 'rxjs';
import {distinctUntilChanged, filter, mapTo, startWith, switchMap, take} from 'rxjs/operators';

// @dynamic
@Injectable({
    providedIn: 'root'
})
export class NcpHoveredService {
    private readonly documentEvents$: Observable<Event>;

    constructor(
        @Inject(DOCUMENT) documentRef: Document,
        @Inject(NgZone) private readonly ngZone: NgZone,
    ) {
        this.documentEvents$ = merge(
            typedFromEvent(documentRef, 'mousemove'),
            typedFromEvent(documentRef, 'touchend', {capture: true}),
        );
    }

    public detectHoverEvents$(
        target: Element,
        options: AddEventListenerOptions = {passive: true},
    ): Observable<boolean> {
        return merge(
            typedFromEvent(target, 'mouseenter', options),
            typedFromEvent(target, 'touchstart', options),
        ).pipe(
            switchMap(() =>
                merge(
                    typedFromEvent(target, 'mouseleave', options),
                    this.documentEvents$.pipe(
                        filter((event) => !target.contains(getActualTarget(event))),
                        ncpZoneOptimized(this.ngZone),
                        take(1),
                    ),
                ).pipe(mapTo(false), startWith(true)),
            ),
            distinctUntilChanged()
        );
    }
}
