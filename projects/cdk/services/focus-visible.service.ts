import {ChangeDetectorRef, ElementRef, Inject, Injectable} from '@angular/core';
import {focusVisibleObservable, ncpWatch} from '@ncp-tools/cdk/observables';
import {Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NcpDestroyService} from './destroy.service';

@Injectable()
export class NcpFocusVisibleService extends Observable<boolean> {
    private readonly focusVisible$: Observable<boolean>;

    constructor(
        @Inject(ElementRef) {nativeElement}: ElementRef<Element>,
        @Inject(ChangeDetectorRef) changeDetectorRef: ChangeDetectorRef,
        @Inject(NcpDestroyService) destroy$: Observable<void>
    ) {
        super((subscriber) => this.focusVisible$.subscribe(subscriber));

        this.focusVisible$ = focusVisibleObservable(nativeElement).pipe(
            ncpWatch(changeDetectorRef),
            takeUntil(destroy$),
        );
    }
}
