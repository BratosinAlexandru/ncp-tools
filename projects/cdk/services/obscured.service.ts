import {ElementRef, Inject, Injectable, NgZone, Self} from '@angular/core';
import {ncpZoneOptimized} from '@ncp-tools/cdk/observables';
import {getElementObscurers} from '@ncp-tools/cdk/utils/dom';
import {ANIMATION_FRAME, WINDOW} from '@ng-web-apis/common';
import {fromEvent, merge, Observable} from 'rxjs';
import {delay, distinctUntilChanged, map, startWith, takeUntil, throttleTime} from 'rxjs/operators';
import {NcpDestroyService} from './destroy.service';
import {NcpParentsScrollService} from './partents-scroll.service';

export const POLLING_TIME = 1000 / 15;

// @dynamic
@Injectable()
export class NcpObscuredService extends Observable<null | ReadonlyArray<Element>> {
    private obscured$: Observable<null | ReadonlyArray<Element>>;

    constructor(
        @Inject(NcpParentsScrollService)
        @Self()
        parentsScroll$: NcpParentsScrollService,
        @Inject(ElementRef) {nativeElement}: ElementRef<Element>,
        @Inject(NgZone) ngZone: NgZone,
        @Inject(WINDOW) windowRef: Window,
        @Inject(NcpDestroyService) destroy$: Observable<void>,
        @Inject(ANIMATION_FRAME) animationFrame$: Observable<number>,
    ) {
        super((subscriber) => this.obscured$.subscribe(subscriber));
        this.obscured$ = merge(
            merge(parentsScroll$, fromEvent(windowRef, 'resize')).pipe(delay(0)),
            animationFrame$.pipe(throttleTime(POLLING_TIME)),
        ).pipe(
            map(() => getElementObscurers(nativeElement)),
            startWith(null),
            distinctUntilChanged(),
            ncpZoneOptimized(ngZone),
            takeUntil(destroy$),
        );
    }
}
