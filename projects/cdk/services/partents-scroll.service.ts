import {ElementRef, Inject, Injectable} from '@angular/core';
import {typedFromEvent} from '@ncp-tools/cdk/observables';
import {WINDOW} from '@ng-web-apis/common';
import {defer, merge, Observable} from 'rxjs';

// @dynamic
@Injectable()
export class NcpParentsScrollService extends Observable<Event> {
    private readonly callback$: Observable<Event>;

    constructor(
        @Inject(ElementRef) {nativeElement}: ElementRef<Element>,
        @Inject(WINDOW) windowRef: Window,
    ) {
        super((subscriber) => this.callback$.subscribe(subscriber));
        this.callback$ = defer(() => {
            const eventTargets: Array<Element | Window> = [windowRef, nativeElement];

            while (nativeElement.parentElement) {
                nativeElement = nativeElement.parentElement;
                eventTargets.push(nativeElement);
            }

            return merge<Event>(
                ...eventTargets.map<Observable<Event>>(element =>
                    typedFromEvent(element, 'scroll')
                )
            );
        });
    }
}
