import {InjectionToken} from '@angular/core';
import {NcpFocusableAccessor} from '@ncp-tools/cdk/interfaces';

export const NCP_FOCUSABLE_ACCESSOR = new InjectionToken<NcpFocusableAccessor>(
    'a component that can be focused',
);
