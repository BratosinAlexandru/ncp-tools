import {OnChanges} from '@angular/core';
import {Subject} from 'rxjs';

export abstract class NcpController implements OnChanges {
    readonly changes$ = new Subject<void>();

    ngOnChanges(): void {
        this.changes$.next();
    }
}
